module gitlab.com/rhinestone-project/go-rsbuild

go 1.14

require (
	gitlab.com/egor9814/go-serialize v1.1.1
	gitlab.com/rhinestone-project/go-rsvm v0.2.8
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
