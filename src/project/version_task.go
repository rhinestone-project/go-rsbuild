package project

import (
	"fmt"
	"gitlab.com/rhinestone-project/go-rsbuild/src/version"
	"gopkg.in/yaml.v3"
)

type versionTask struct{}

func (vt *versionTask) Name() string {
	return "version"
}

func (vt *versionTask) Description() string {
	return "RhineStone Builder version"
}

func (vt *versionTask) Help() string {
	return "Print version of rsbuild"
}

func (vt *versionTask) Parse(_ *yaml.Node) error {
	return nil
}

func (vt *versionTask) Execute(_ *Project, _ []string) error {
	fmt.Println(version.SemVer)
	return nil
}
