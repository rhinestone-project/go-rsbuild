package project

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"strings"
)

type tasks struct{}

func (t *tasks) Name() string {
	return "tasks"
}

func (t *tasks) Description() string {
	return "Project tasks"
}

func (t *tasks) Help() string {
	return "Print project tasks"
}

func (t *tasks) Parse(_ *yaml.Node) error {
	return nil
}

func (t *tasks) Execute(p *Project, _ []string) error {
	result := make([]string, len(p.Tasks))
	desc := make([]string, len(p.Tasks))
	idx := 0
	maxLen := 0
	for name, task := range p.Tasks {
		result[idx] = name
		desc[idx] = task.Description()
		if l := len(name); l > maxLen {
			maxLen = l
		}
		idx++
	}
	if tmp := maxLen % 4; tmp != 0 {
		maxLen += tmp
	}
	for idx, it := range result {
		result[idx] = it + strings.Repeat(" ", maxLen-len(it))
	}

	for i, it := range result {
		fmt.Println(it + "\t- " + desc[i])
	}

	return nil
}
