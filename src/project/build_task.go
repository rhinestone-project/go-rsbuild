package project

import (
	"errors"
	"fmt"
	"gitlab.com/egor9814/go-serialize"
	"gitlab.com/rhinestone-project/go-rsbuild/src/ast"
	"gitlab.com/rhinestone-project/go-rsbuild/src/bytecode"
	"gitlab.com/rhinestone-project/go-rsbuild/src/compiler"
	errors2 "gitlab.com/rhinestone-project/go-rsbuild/src/errors"
	"gitlab.com/rhinestone-project/go-rsbuild/src/lexer"
	"gitlab.com/rhinestone-project/go-rsbuild/src/parser"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	vmbytecode "gitlab.com/rhinestone-project/go-rsvm/src/bytecode"
	"gopkg.in/yaml.v3"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type buildTask struct {
	debug     bool
	sourceSet []string
	output    struct {
		dir  string
		name string
	}
}

func newBuildTask() *buildTask {
	return &buildTask{
		debug:     false,
		sourceSet: []string{"src"},
		output: struct {
			dir  string
			name string
		}{
			dir:  "out",
			name: "a",
		},
	}
}

func (bt *buildTask) Name() string {
	return "build"
}

func (bt *buildTask) Description() string {
	return "Build current project"
}

func (bt *buildTask) Help() string {
	return "Build current project"
}

func (bt *buildTask) Parse(config *yaml.Node) error {
	var err error
	var debugNode, sourcesNode, outputNode *yaml.Node
	for i := 0; i < len(config.Content); i += 2 {
		switch strings.ToLower(config.Content[i].Value) {
		case "debug":
			debugNode = config.Content[i+1]
		case "sourceset":
			sourcesNode = config.Content[i+1]
		case "output":
			outputNode = config.Content[i+1]
		default:
			return errors.New("unsupported build field '" + config.Content[i].Value + "'")
		}
	}

	if debugNode != nil {
		bt.debug, err = strconv.ParseBool(debugNode.Value)
		if err != nil {
			return err
		}
	}

	if sourcesNode != nil {
	L:
		for _, it := range sourcesNode.Content {
			val := it.Value
			for _, elem := range bt.sourceSet {
				if elem == val {
					continue L
				}
			}
			bt.sourceSet = append(bt.sourceSet, val)
		}
	}

	if outputNode != nil {
		var dirNode, nameNode *yaml.Node
		for i := 0; i < len(outputNode.Content); i += 2 {
			switch strings.ToLower(outputNode.Content[i].Value) {
			case "dir", "directory":
				dirNode = outputNode.Content[i+1]
			case "name":
				nameNode = outputNode.Content[i+1]
			default:
				return errors.New("unsupported output field '" + outputNode.Content[i].Value + "'")
			}
		}

		if dirNode != nil {
			bt.output.dir = dirNode.Value
		}

		if nameNode != nil && len(nameNode.Value) > 0 {
			bt.output.name = nameNode.Value
		}
	}

	return nil
}

func scanSourceFiles(files *[]string) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filepath.Ext(info.Name()) == ".sors" {
			*files = append(*files, path)
		}
		return nil
	}
}

func getSources(sourceSet []string) ([]string, error) {
	result := make([]string, 0, len(sourceSet))
	for _, sourceDir := range sourceSet {
		if _, err := os.Stat(sourceDir); err != nil {
			continue
		}
		if err := filepath.Walk(sourceDir, scanSourceFiles(&result)); err != nil {
			return nil, err
		}
	}
	return result, nil
}

func readSource(sourceFile string) ([]byte, error) {
	f, err := os.Open(sourceFile)
	if err != nil {
		return nil, err
	}
	defer func(c io.Closer) {
		if err := c.Close(); err != nil {
			println("warning: error while closing file '" + sourceFile + "'")
		}
	}(f)

	data, err := util.ReadSourceData(f)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func parse(sourceFile string, maxErrorsCount int) (*ast.CompilationUnit, error) {
	src, err := readSource(sourceFile)
	if err != nil {
		return nil, err
	}
	ts, err := lexer.NewLexer(src)
	if err != nil {
		return nil, err
	}

	fileName := filepath.Base(sourceFile)
	if ext := filepath.Ext(fileName); len(ext) > 0 {
		fileName = fileName[:len(fileName)-len(ext)]
	}

	unit, err := parser.Parse(ts, fileName, maxErrorsCount)
	if err != nil {
		return nil, nil
	}

	return unit, nil
}

type parseResult struct {
	units  chan *ast.CompilationUnit
	errors chan error
}

func parseFiles(sourceFiles []string, maxErrorsCount int) *parseResult {
	out := &parseResult{
		units:  make(chan *ast.CompilationUnit),
		errors: make(chan error),
	}
	for _, it := range sourceFiles {
		go func(path string) {
			unit, err := parse(path, maxErrorsCount)
			out.units <- unit
			out.errors <- err
		}(it)
	}
	return out
}

func saveFile(path string, data []byte) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer func(c io.Closer) {
		if err := c.Close(); err != nil {
			println("warning: error while closing file '" + path + "'")
		}
	}(f)

	_, err = f.Write(data)
	return err
}

func (bt *buildTask) Execute(p *Project, _ []string) error {
	fmt.Println("building project \"" + p.Name + "\"")

	wd := p.WorkSpace
	sourceSet := make([]string, len(bt.sourceSet))
	for idx, it := range bt.sourceSet {
		sourceSet[idx] = filepath.Join(wd, it)
	}
	output := wd
	if len(bt.output.dir) != 0 {
		output = filepath.Join(output, bt.output.dir)
	}
	if _, err := os.Stat(output); err != nil {
		if err := os.MkdirAll(output, 0755); err != nil {
			return err
		}
	}
	if _, err := os.Stat(output); err != nil {
		return errors.New("cannot create output directory")
	}
	output = filepath.Join(output, bt.output.name+".rsbin")

	sourceFiles, err := getSources(sourceSet)
	if err != nil {
		return err
	} else if len(sourceFiles) == 0 {
		return errors.New("no source files")
	} else if len(sourceFiles) > 1 {
		// TODO: temporary multiple files not supported
		return errors.New("multiple source files not supported")
	}

	errorList := errors2.NewErrorList()

	fmt.Println("parsing sources...")
	units := make([]*ast.CompilationUnit, 0, len(sourceFiles))
	input := parseFiles(sourceFiles, 10)
	for it := range input.units {
		err := <-input.errors
		if err != nil {
			errorList.Add(err.Error(), nil)
		}
		units = append(units, it)
		if len(units) == len(sourceFiles) {
			close(input.units)
			close(input.errors)
		}
	}
	if err := errorList.Error(); err != nil {
		return err
	}

	globals := ast.NewScope(nil)
	/// TODO: in feature remove this @{
	tmp := ast.NewSymbol("tmpi", ast.SymbolTypeVar)
	tmp.Data = rs_api.TypeInt
	globals.Put(tmp)
	tmp = ast.NewSymbol("tmps", ast.SymbolTypeVar)
	tmp.Data = rs_api.TypeString
	globals.Put(tmp)
	/// TODO: }@

	fmt.Println("resolving symbols...")
	for _, unit := range units {
		if !unit.Resolve(globals) {
			unresolved := make([]string, len(unit.Unresolved))
			for idx, it := range unit.Unresolved {
				unresolved[idx] = it.Value
			}
			errorList.Add("unresolved symbols: "+strings.Join(unresolved, ", "), &util.FilePosition{
				Row:      1,
				FilePath: unit.Name,
			})
		}
	}
	if err := errorList.Error(); err != nil {
		return err
	}

	fmt.Println("compile sources...")
	c := compiler.NewCompiler(false)
	for _, unit := range units {
		if !unit.Compile(c) {
			errorList.Add("compilation errors: "+strings.Join(c.GetErrors(), "\n  "), &util.FilePosition{
				Row:      0,
				FilePath: unit.Name,
			})
		}
	}
	if err := errorList.Error(); err != nil {
		return err
	}

	fmt.Println("generating bytecode...")
	bc := bytecode.New()
	for _, unit := range units {
		if !unit.Generate(c, bc) {
			errorList.Add("generation errors: "+strings.Join(c.GetErrors(), "\n  "), &util.FilePosition{
				Row:      0,
				FilePath: unit.Name,
			})
		}
	}
	if err := errorList.Error(); err != nil {
		return err
	}
	bc.End()

	header := &vmbytecode.Header{
		Magic: 940136395564325404,
		Flags: vmbytecode.HeaderNoFlags,
	}

	constPool := c.ConstPool().Data()
	byteCode := bc.Data()

	ser, result := go_serialize.NewByteArraySerializer(uint(20 + len(constPool) + len(byteCode)))

	if err := ser.Serializable(header); err != nil {
		return err
	} else if err := ser.Int(len(constPool)); err != nil {
		return err
	} else if err := ser.Bytes(constPool); err != nil {
		return err
	} else if err := ser.Int(len(byteCode)); err != nil {
		return err
	} else if err := ser.Bytes(byteCode); err != nil {
		return err
	}

	err = saveFile(output, *result)
	if err != nil {
		return err
	}

	fmt.Println("building successful!")
	rel, err := filepath.Rel(wd, output)
	if err != nil {
		return err
	}
	fmt.Println("executable file saved to " + rel)
	return nil
}
