package project

import (
	"fmt"
	"gopkg.in/yaml.v3"
)

type helpTask struct{}

func (h *helpTask) Name() string {
	return "help"
}

func (h *helpTask) Description() string {
	return "Print help"
}

func (h *helpTask) Help() string {
	return "Print general help message or message for specified task\n" +
		"usage \"rsbuild help [<taskName>]"
}

func (h *helpTask) Parse(_ *yaml.Node) error {
	return nil
}

func (h *helpTask) Execute(p *Project, args []string) error {
	if len(args) == 0 {
		fmt.Println("RhineStone Builder usage:")
		fmt.Println("  rsbuild <task> [args]")
		fmt.Println("  type \"rsbuild tasks\" for print tasks")
		fmt.Println("  type \"rsbuild help\" for print help")
		fmt.Println("  type \"rsbuild help <task_name>\" for print help for specified task")
	} else {
		taskName := args[0]
		if task, ok := p.Tasks[taskName]; ok {
			fmt.Println(task.Help())
		} else {
			fmt.Println("task \"" + taskName + "\" not exists")
			fmt.Println("type \"rsbuild tasks\"")
		}
	}
	return nil
}
