package project

import "gopkg.in/yaml.v3"

type ITask interface {
	Name() string
	Description() string
	Help() string
	Parse(config *yaml.Node) error
	Execute(p *Project, args []string) error
}
