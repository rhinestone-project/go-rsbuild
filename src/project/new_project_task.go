package project

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"os"
	"path/filepath"
)

type newProject struct{}

func (np *newProject) Name() string {
	return "new"
}

func (np *newProject) Description() string {
	return "Create new project"
}

func (np *newProject) Help() string {
	return "Create new project in current directory\n" +
		"usage: rsbuild new <project_name>"
}

func (np *newProject) Parse(_ *yaml.Node) error {
	return nil
}

func (np *newProject) Execute(p *Project, args []string) error {
	if len(args) == 0 {
		fmt.Println("project name unspecified")
		fmt.Println("type \"rsbuild help new\"")
	} else {
		fmt.Println("creating project \"" + args[0] + "\" in \"" + p.WorkSpace + "\"...")
		projectDir := filepath.Join(p.WorkSpace, args[0])
		if err := os.MkdirAll(projectDir, 0755); err != nil {
			return err
		}
		if err := os.MkdirAll(filepath.Join(projectDir, "src"), 0755); err != nil {
			return err
		}
		if err := os.MkdirAll(filepath.Join(projectDir, "out"), 0755); err != nil {
			return err
		}

		if f, err := os.Create(filepath.Join(projectDir, "src", "main.sors")); err != nil {
			return err
		} else {
			defer func() {
				if err := f.Close(); err != nil {
					fmt.Println("error while closing template source file:", err.Error())
				}
			}()
			if _, err := f.WriteString("\ntmpi++\n"); err != nil {
				return err
			}
		}

		if f, err := os.Create(filepath.Join(projectDir, "rsbuild.yml")); err != nil {
			return err
		} else {
			defer func() {
				if err := f.Close(); err != nil {
					fmt.Println("error while closing template build file:", err.Error())
				}
			}()
			if _, err := f.WriteString(`project:
  name: "` + filepath.Base(args[0]) + `"

build:
  debug: true
  sourceSet:
    - "src"
  output:
    dir: "out"
    name: "a"
`); err != nil {
				return err
			}
		}

		fmt.Println("done!")
	}
	return nil
}
