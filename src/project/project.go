package project

import (
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"strings"
)

type Project struct {
	WorkSpace string
	Name      string
	Tasks     map[string]ITask
}

func New(workspace string) *Project {
	p := &Project{
		WorkSpace: workspace,
		Name:      "",
		Tasks:     make(map[string]ITask),
	}
	p.Tasks["tasks"] = &tasks{}
	p.Tasks["help"] = &helpTask{}
	p.Tasks["version"] = &versionTask{}
	p.Tasks["new"] = &newProject{}
	return p
}

func (p *Project) parseProject(node *yaml.Node) error {
	for i := 0; i < len(node.Content); i += 2 {
		switch strings.ToLower(node.Content[i].Value) {
		case "name":
			if len(node.Content[i+1].Value) == 0 {
				p.Name = "unnamed"
			} else {
				p.Name = node.Content[i+1].Value
			}
		default:
			return errors.New("unknown project field '" + node.Content[i].Value + "'")
		}
	}
	return nil
}

func (p *Project) parseBuildTask(node *yaml.Node) error {
	t := newBuildTask()
	if err := t.Parse(node); err != nil {
		return err
	}
	p.Tasks["build"] = t
	return nil
}

func (p *Project) Parse(node *yaml.Node) error {
	for i := 0; i < len(node.Content); i += 2 {
		switch strings.ToLower(node.Content[i].Value) {
		case "project":
			if err := p.parseProject(node.Content[i+1]); err != nil {
				return err
			}
		case "build":
			if err := p.parseBuildTask(node.Content[i+1]); err != nil {
				return err
			}
		default:
			return errors.New("custom tasks not support yet")
		}
	}
	return nil
}

func (p *Project) Execute(args []string) error {
	if len(args) == 0 {
		fmt.Println("type \"rsbuild help\"")
	} else {
		taskName := args[0]
		if task, ok := p.Tasks[taskName]; ok {
			return task.Execute(p, args[1:])
		} else {
			fmt.Println("task \"" + taskName + "\" not exists")
			fmt.Println("type \"rsbuild tasks\"")
		}
	}
	return nil
}
