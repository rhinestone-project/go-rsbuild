package project

import (
	"fmt"
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"os"
	"path/filepath"
)

func Execute(args []string) error {
	workspace, err := filepath.Abs("")
	if err != nil {
		return err
	}
	p := New(workspace)

	buildFile := filepath.Join(workspace, "rsbuild.yml")
	if _, err := os.Stat(buildFile); err == nil {
		f, err := os.Open(buildFile)
		if err != nil {
			return err
		}
		defer func() {
			if err := f.Close(); err != nil {
				fmt.Println("error while closing build file:", err.Error())
			}
		}()

		data, err := ioutil.ReadAll(f)
		if err != nil {
			return err
		}
		var node yaml.Node
		err = yaml.Unmarshal(data, &node)
		if err != nil {
			return err
		}

		err = p.Parse(node.Content[0])
		if err != nil {
			return err
		}
	}

	return p.Execute(args)
}
