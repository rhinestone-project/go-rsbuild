package lexer

type Mode byte

const (
	ModeNone Mode = 0

	ModeSkipWhiteSpaces Mode = 1 << iota

	// TODO: implement comments
	//ModeSkipSingleLineComments
	//ModeSkipMultiLineComments

	// TODO: implement doc comments
	//ModeSkipDocComments

	//ModeSkipComments = ModeSkipSingleLineComments | ModeSkipMultiLineComments

	ModeDefault = ModeSkipWhiteSpaces
)
