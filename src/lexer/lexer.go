package lexer

import (
	"errors"
	errors2 "gitlab.com/rhinestone-project/go-rsbuild/src/errors"
	"gitlab.com/rhinestone-project/go-rsbuild/src/token"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"strconv"
	"strings"
)

type Lexer struct {
	mode Mode
	b    *buffer
	pos  util.Position
}

func NewLexer(data []byte) (*Lexer, error) {
	b, err := makeBuffer(data)
	if err != nil {
		return nil, err
	}
	b.tabSize = 4
	return &Lexer{
		mode: ModeDefault,
		b:    b,
		pos: util.Position{
			Offset: 0,
			Row:    1,
			Col:    1,
		},
	}, nil
}

func (l *Lexer) Mode() *Mode {
	return &l.mode
}

func (l *Lexer) TabSize() *int {
	return &l.b.tabSize
}

func (l *Lexer) tokenOf(count int, t token.Type) *token.Token {
	buf := l.b.get(count)
	pos := l.b.consume(buf, l.pos)
	return &token.Token{
		Text:  buf,
		Type:  t,
		Where: pos,
	}
}

func (l *Lexer) tokenOfType(t token.Type, consumeBuffer bool) *token.Token {
	var pos util.PositionRange
	if consumeBuffer {
		pos = l.b.consume(t.Repr(), l.pos)
	} else {
		pos = l.pos.RangeTo(l.pos)
	}
	return &token.Token{
		Text:  t.Repr(),
		Type:  t,
		Where: pos,
	}
}

func (l *Lexer) read(t token.Type, predicate func(ch char) bool) *token.Token {
	currentPos := 1
	for {
		chr := l.b.at(currentPos)
		currentPos++
		if !predicate(chr) {
			break
		}
	}
	return l.tokenOf(currentPos-1, t)
}

func (l *Lexer) readInteger() *token.Token {
	return l.read(token.TypeIntLiteral, func(ch char) bool {
		return ch == '_' || ch.isDigit()
	})
}

func (l *Lexer) readWS() *token.Token {
	return l.read(token.TypeWhiteSpace, func(ch char) bool {
		return ch.isWhiteSpace()
	})
}

func (l *Lexer) readString() (*token.Token, int, error) {
	multiLine := false
	currentPos := 1
	if l.b.at(currentPos) == '"' {
		if l.b.at(currentPos+1) == '"' {
			multiLine = true
			currentPos += 2
		} else {
			return l.tokenOf(2, token.TypeStringLiteral), 0, nil
		}
	}

	skipChar := false
	for {
		if skipChar {
			skipChar = false
		} else {
			chr := l.b.at(currentPos)
			if chr == '"' {
				if multiLine {
					if l.b.at(currentPos+1) == '"' && l.b.at(currentPos+2) == '"' {
						currentPos += 3
						return l.tokenOf(currentPos, token.TypeStringLiteral), 0, nil
					}
				} else {
					return l.tokenOf(currentPos+1, token.TypeStringLiteral), 0, nil
				}
			} else if chr == 0 {
				return nil, currentPos, errors.New(errors2.CodeUnexpectedEOF.String())
			} else if chr == '\r' || chr == '\n' {
				if !multiLine {
					return nil, currentPos, errors.New(errors2.CodeUnexpectedEOL.String())
				}
			} else if chr == '\\' {
				skipChar = true
			}
		}
		currentPos++
	}
}

func isIdStart(c char) bool {
	return c.isLetter() || c == '_'
}

func isIdPart(c char) bool {
	return isIdStart(c) || c.isDigit()
}

func (l *Lexer) readId() *token.Token {
	return l.read(token.TypeIdentifier, isIdPart)
}

func isOperator(chr char) bool {
	return strings.ContainsRune("~!%^&*()-+={}[]|\\/<>,.;:?", rune(chr))
}

var singleCharOperators = map[char]token.Type{
	'(': token.TypeLParen,
	')': token.TypeRParen,
}

func (l *Lexer) readOperator(chr char) (*token.Token, error) {
	if t, ok := singleCharOperators[chr]; ok {
		return l.tokenOfType(t, true), nil
	}

	switch chr {
	case '+':
		chr = l.b.at(1)
		if chr == '+' {
			return l.tokenOfType(token.TypePlus2, true), nil
		}
		return l.tokenOfType(token.TypePlus, true), nil
	case '-':
		chr = l.b.at(1)
		if chr == '-' {
			return l.tokenOfType(token.TypeMinus2, true), nil
		}
		return l.tokenOfType(token.TypeMinus, true), nil
	default:
		return nil, errors.New("not supported operator '" + string(chr) + "' at " + strconv.Itoa(l.pos.Col))
	}
}

func isEOL(chr char) bool {
	return chr == '\n' || chr == '\r'
}

func (l *Lexer) readEOL() *token.Token {
	return l.read(token.TypeEOL, isEOL)
}

func (l *Lexer) readToken() (*token.Token, error) {
	chr := l.b.at(0)
	switch {
	case chr.isDigit():
		return l.readInteger(), nil
	case chr.isWhiteSpace():
		return l.readWS(), nil
	case chr == '"':
		t, errPos, err := l.readString()
		if err != nil {
			buf := l.b.get(errPos)
			end := l.b.consume(buf, l.pos).End
			return nil, errors.New(err.Error() + " at " + strconv.Itoa(end.Col))
		} else {
			return t, nil
		}
	case isIdStart(chr):
		return l.readId(), nil
	case isOperator(chr):
		return l.readOperator(chr)
	case isEOL(chr):
		return l.readEOL(), nil
	case chr == 0:
		return l.tokenOfType(token.TypeEOF, false), nil
	default:
		return nil, errors.New("unrecognized character '" + chr.String() + "' at " + strconv.Itoa(l.pos.Col))
	}
}

func (l *Lexer) Next(errorHandler errors2.ErrorHandler) token.Token {
	for {
		t, err := l.readToken()
		if err != nil {
			errorHandler(err.Error(), &l.pos)
			return *l.tokenOfType(token.TypeInvalid, false)
		}
		l.pos = t.Where.End
		if t.Type.IsWhiteSpace() && ((l.mode & ModeSkipWhiteSpaces) != 0) {
			continue
		} else {
			return *t
		}
	}
}
