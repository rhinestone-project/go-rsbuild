package lexer

import (
	"errors"
	"unicode/utf8"
)

type scanner struct {
	data        []byte
	bytesOffset int
	charOffset  int
	current     rune
}

func newScanner(data []byte) *scanner {
	return &scanner{
		data:        data,
		bytesOffset: 0,
		charOffset:  0,
		current:     0,
	}
}

func (cs *scanner) readRune() (rune, error) {
	if cs.bytesOffset < len(cs.data) {
		r, rs := rune(cs.data[cs.bytesOffset]), 1
		switch {
		case r == 0:
			return 0, errors.New("invalid character \\0")
		case r >= utf8.RuneSelf:
			r, rs = utf8.DecodeRune(cs.data[cs.bytesOffset:])
			switch {
			case r == utf8.RuneError && rs == 1:
				return 0, errors.New("invalid UTF-8 encoding")
			case r == 0xFEFF && cs.bytesOffset > 0:
				return 0, errors.New("invalid BOM")
			}
		}
		cs.bytesOffset += rs
		cs.current = r
		cs.charOffset++
	} else {
		cs.current = 0
	}
	return cs.current, nil
}

func (cs *scanner) readChar() (char, error) {
	r, err := cs.readRune()
	if err != nil {
		return 0, err
	}
	return char(r), nil
}
