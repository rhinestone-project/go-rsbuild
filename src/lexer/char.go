package lexer

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"unicode"
)

type char rune

func (chr char) String() string {
	return "'" + util.EscapedStringOfRune(rune(chr)) + "'"
}

func (chr char) isLetter() bool {
	return unicode.IsLetter(rune(chr))
}

func (chr char) isDigit() bool {
	return unicode.IsDigit(rune(chr))
}

func (chr char) isHexDigit() bool {
	return ('0' <= chr && chr <= '9') ||
		('a' <= chr && chr <= 'f') || ('A' <= chr && chr <= 'F')
}

func (chr char) isOctDigit() bool {
	return '0' <= chr && chr <= '7'
}

func (chr char) isBinDigit() bool {
	return chr == '0' || chr == '1'
}

func (chr char) isSpace() bool {
	return chr == ' '
}

func (chr char) isTab() bool {
	return chr == '\t'
}

func (chr char) isNewLine() bool {
	return chr == '\n' || chr == '\r'
}

func (chr char) isWhiteSpace() bool {
	return chr.isSpace() || chr.isTab()
}

type charArray []char

func (ca charArray) toRunes() []rune {
	result := make([]rune, len(ca))
	for idx, it := range ca {
		result[idx] = rune(it)
	}
	return result
}

func (ca charArray) String() string {
	return string(ca.toRunes())
}
