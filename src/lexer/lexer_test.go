package lexer

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/errors"
	"gitlab.com/rhinestone-project/go-rsbuild/src/token"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"reflect"
	"testing"
)

func newTestLexer(source string) *Lexer {
	if d, err := util.ReadSourceData(source); err != nil {
		panic(err)
	} else if l, err := NewLexer(d); err != nil {
		panic(err)
	} else {
		*l.Mode() = ModeNone
		return l
	}
}

func TestLexer_Mode(t *testing.T) {}

func TestLexer_Next(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		source  string
		want    token.Token
		wantErr bool
	}{
		{
			name:   "check eof",
			source: "",
			want: token.Token{
				Text: "'\\0'",
				Type: token.TypeEOF,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			el := errors.NewErrorListWithCapacity(1)
			l := newTestLexer(tt.source)
			if got, err := l.Next(el.Add), el.Error(); (err != nil) != tt.wantErr {
				t.Errorf("Next() error = %v, wantErr %v", err, tt.wantErr)
			} else if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Next() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLexer_TabSize(t *testing.T) {}

func TestLexer_read(t *testing.T) {}

func TestLexer_readInteger(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name   string
		source string
		want   *token.Token
	}{
		{
			name:   "zero",
			source: "0.",
			want: &token.Token{
				Text: "0",
				Type: token.TypeIntLiteral,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 1,
						Row:    1,
						Col:    2,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := newTestLexer(tt.source)
			got := l.readInteger()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readInteger() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLexer_readToken(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		source  string
		want    *token.Token
		wantErr bool
	}{
		{
			name:   "int",
			source: "12_34",
			want: &token.Token{
				Text: "12_34",
				Type: token.TypeIntLiteral,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 5,
						Row:    1,
						Col:    6,
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "ws",
			source: " \t ",
			want: &token.Token{
				Text: " \t ",
				Type: token.TypeWhiteSpace,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 3,
						Row:    1,
						Col:    6,
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "string",
			source: `"text"`,
			want: &token.Token{
				Text: `"text"`,
				Type: token.TypeStringLiteral,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 6,
						Row:    1,
						Col:    7,
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "string multiline",
			source: "\"\"\"te\nxt\"\"\"",
			want: &token.Token{
				Text: "\"\"\"te\nxt\"\"\"",
				Type: token.TypeStringLiteral,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 11,
						Row:    2,
						Col:    6,
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "id",
			source: "id",
			want: &token.Token{
				Text: "id",
				Type: token.TypeIdentifier,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 2,
						Row:    1,
						Col:    3,
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "eof",
			source: "",
			want: &token.Token{
				Text: "'\\0'",
				Type: token.TypeEOF,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := newTestLexer(tt.source)
			got, err := l.readToken()
			if (err != nil) != tt.wantErr {
				t.Errorf("readToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readToken() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLexer_readWS(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name   string
		source string
		want   *token.Token
	}{
		{
			name:   "space",
			source: " ",
			want: &token.Token{
				Text: " ",
				Type: token.TypeWhiteSpace,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 1,
						Row:    1,
						Col:    2,
					},
				},
			},
		},
		{
			name:   "tab",
			source: "\t",
			want: &token.Token{
				Text: "\t",
				Type: token.TypeWhiteSpace,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 1,
						Row:    1,
						Col:    5,
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := newTestLexer(tt.source)
			got := l.readWS()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readWS() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLexer_readString(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		source  string
		want    *token.Token
		wantErr bool
	}{
		{
			name:   "single",
			source: "\"it is text\"",
			want: &token.Token{
				Text: "\"it is text\"",
				Type: token.TypeStringLiteral,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 12,
						Row:    1,
						Col:    13,
					},
				},
			},
			wantErr: false,
		},
		{
			name:    "single with slash n",
			source:  "\"it is\ntext\"",
			want:    nil,
			wantErr: true,
		},
		{
			name:   "multi",
			source: "\"\"\"it\nis\ntext\"\"\"",
			want: &token.Token{
				Text: "\"\"\"it\nis\ntext\"\"\"",
				Type: token.TypeStringLiteral,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 16,
						Row:    3,
						Col:    8,
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := newTestLexer(tt.source)
			got, _, err := l.readString()
			if (err != nil) != tt.wantErr {
				t.Errorf("readString() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readString() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLexer_readId(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		source  string
		want    *token.Token
		wantErr bool
	}{
		{
			name:   "simple",
			source: "_itIs_ID2",
			want: &token.Token{
				Text: "_itIs_ID2",
				Type: token.TypeIdentifier,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 9,
						Row:    1,
						Col:    10,
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "not id",
			source: "2_itIs_ID2",
			want: &token.Token{
				Text: "2_itIs_ID2",
				Type: token.TypeIdentifier, // because first char skipped
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 10,
						Row:    1,
						Col:    11,
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := newTestLexer(tt.source)
			got := l.readId()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readId() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLexer_readOperator(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		source  string
		want    *token.Token
		wantErr bool
	}{
		{
			name:   "left paren",
			source: "()",
			want: &token.Token{
				Text: "(",
				Type: token.TypeLParen,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 1,
						Row:    1,
						Col:    2,
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "+++",
			source: "+++",
			want: &token.Token{
				Text: "++",
				Type: token.TypePlus2,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 2,
						Row:    1,
						Col:    3,
					},
				},
			},
			wantErr: false,
		},
		{
			name:   "-++",
			source: "-++",
			want: &token.Token{
				Text: "-",
				Type: token.TypeMinus,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 1,
						Row:    1,
						Col:    2,
					},
				},
			},
			wantErr: false,
		},
		{
			name:    "not operator",
			source:  "7",
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			l := newTestLexer(tt.source)
			got, err := l.readOperator(l.b.at(0))
			if (err != nil) != tt.wantErr {
				t.Errorf("readOperator() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("readOperator() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLexer_tokenOf(t *testing.T) {}

func TestLexer_tokenOfType(t *testing.T) {}

func TestNewLexer(t *testing.T) {}
