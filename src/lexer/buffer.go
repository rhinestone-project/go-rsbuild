package lexer

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
)

type buffer struct {
	buf     charArray
	tabSize int
}

func makeBuffer(data []byte) (*buffer, error) {
	chars := make(charArray, len(data))
	idx := 0
	s := newScanner(data)
	for {
		if c, err := s.readChar(); err != nil {
			return nil, err
		} else if c == 0 {
			break
		} else {
			chars[idx] = c
			idx++
		}
	}
	return &buffer{buf: chars}, nil
}

func (b *buffer) get(count int) string {
	if count > len(b.buf) {
		return b.buf.String()
	}
	return b.buf[:count].String()
}

func (b *buffer) at(pos int) char {
	if pos < len(b.buf) {
		return b.buf[pos]
	} else {
		return 0
	}
}

func (b *buffer) consume(s string, begin util.Position) util.PositionRange {
	end := begin
	skipLF := false
	for _, it := range s {
		end.Offset++
		switch it {
		case '\r':
			skipLF = true
			end.Row++
			end.Col = 1
			continue
		case '\n':
			if !skipLF {
				end.Row++
				end.Col = 1
			}
		case '\t':
			col := end.Col - 1
			if rem := col % b.tabSize; rem == 0 {
				col += b.tabSize
			} else {
				col += b.tabSize - rem
			}
			end.Col = col + 1
		default:
			end.Col++
		}
		skipLF = false
	}
	b.buf = b.buf[len(s):]
	return begin.RangeTo(end)
}
