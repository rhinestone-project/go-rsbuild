package parser

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/ast"
	"gitlab.com/rhinestone-project/go-rsbuild/src/errors"
	"gitlab.com/rhinestone-project/go-rsbuild/src/token"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"strings"
)

type parser struct {
	ts             token.ITokenStream
	tok            token.Token
	el             errors.ErrorList
	maxErrorsCount int

	scope      *ast.Scope
	unresolved []*ast.IdExpr
}

func (p *parser) errorAt(pos errors.ErrorPosition, msg string) {
	if p.maxErrorsCount > 0 {
		n := p.el.Len()
		if n > 0 && p.el[n-1].Line() == pos.Line() {
			return
		}
		if n > p.maxErrorsCount {
			panic(stopParsing{})
		}
	}
	p.el.Add(msg, pos)
}

func (p *parser) error(msg string) {
	p.errorAt(&p.tok.Where.Begin, msg)
}

func (p *parser) errorExpectedAt(pos errors.ErrorPosition, msg string) string {
	msg = "expected " + msg + ", but found \"" + p.tok.EscapedText() + "\" (" + p.tok.Type.Name() + ")"
	p.errorAt(pos, msg)
	return msg
}

func (p *parser) errorExpected(msg string) string {
	return p.errorExpectedAt(&p.tok.Where.Begin, msg)
}

func (p *parser) errorUnexpected(msg string) string {
	msg = "unexpected " + msg
	p.error(msg)
	return msg
}

func (p *parser) expect(types ...token.Type) util.PositionRange {
	pos := p.tok.Where
	found := false
	for _, tt := range types {
		if p.tok.Type == tt {
			found = true
			break
		}
	}
	if !found {
		if len(types) == 1 {
			p.errorExpected(types[0].Name())
		} else {
			typesNames := make([]string, len(types))
			for idx, it := range types {
				if idx == len(typesNames)-1 {
					typesNames[idx] = "or "
				} else if idx > 0 {
					typesNames[idx] = ", "
				}
				typesNames[idx] += it.Name()
			}
			p.errorExpected(strings.Join(typesNames, ""))
		}
	}
	p.next(true)
	return pos
}

func (p *parser) handleError(message string, where errors.ErrorPosition) {
	p.errorAt(where, message)
}

func assert(condition bool, message string) {
	if !condition {
		panic(newInternalError(message))
	}
}

func (p *parser) next(skipEOL bool) {
	for {
		p.tok = p.ts.Next(p.handleError)
		if !skipEOL || !p.tok.Type.IsEOL() {
			break
		}
	}
}

// scope
func (p *parser) openScope() {
	p.scope = ast.NewScope(p.scope)
}

func (p *parser) closeScope() {
	assert(p.scope != nil, "unbalanced scopes")
	p.scope = p.scope.Parent
}

// symbol resolving and declaring
var unresolved = &ast.Symbol{}

func (p *parser) resolve(e ast.Expression) {
	id, ok := e.(*ast.IdExpr)
	if !ok || id == nil {
		return
	}
	assert(id.Symbol == nil, "identifier already declared or resolved")

	// find symbol
	for s := p.scope; s != nil; s = s.Parent {
		if symbol := s.Find(id.Value); symbol != nil {
			id.Symbol = symbol
			return
		}
	}

	id.Symbol = unresolved
	p.unresolved = append(p.unresolved, id)
}

func (p *parser) declare(pos util.Position, scope *ast.Scope, t ast.SymbolType, id *ast.IdExpr) {
	assert(id.Symbol == nil, "id already declared")
	if id.Value == "_" {
		return
	}
	s := ast.NewSymbol(id.Value, t)
	s.Pos = pos
	id.Symbol = s
	if old := scope.Put(s); old != nil {
		prevDecl := "previously declaration at " + old.Pos.String()
		p.errorAt(&pos, id.Value+" redeclaration in this block\n\t"+prevDecl)
	}
}

// expressions
func (p *parser) id() ast.Expression {
	tok := p.tok
	if tok.Type != token.TypeIdentifier {
		p.errorExpected(token.TypeIdentifier.Name())
		tok.Text = ""
	}
	p.next(true)
	return ast.CreateId(tok.Text, tok.Where)
}

func (p *parser) simple(lhs bool) ast.Expression {
	switch p.tok.Type {
	case token.TypeIntLiteral, token.TypeStringLiteral:
		defer p.next(true)
		lit, err := ast.CreateLiteral(p.tok.Text, p.tok.Type, p.tok.Where)
		if err == nil {
			return lit
		} else {
			p.error(err.Error())
			return ast.CreateInvalidExpr(err.Error(), p.tok.Where)
		}
	case token.TypeIdentifier:
		id := p.id()
		if !lhs {
			p.resolve(id)
		}
		return id
	case token.TypeLParen:
		begin := p.expect(token.TypeLParen).Begin
		e := p.rhs()
		end := p.expect(token.TypeRParen).End
		return ast.CreateParenExpr(begin, end, e)
	default:
		pos := p.expect(token.TypeIntLiteral, token.TypeStringLiteral, token.TypeIdentifier,
			token.TypeLParen)
		return ast.CreateInvalidExpr(p.el.Last(), pos)
	}
}

func (p *parser) postfixUnary(lhs bool) ast.Expression {
	target := p.simple(lhs)

	for {
		switch p.tok.Type {
		case token.TypePlus2, token.TypeMinus2:
			o, pos := p.tok.Type, p.tok.Where.Begin
			p.next(true)
			if lhs {
				p.resolve(target)
			}
			target = ast.CreatePostfixUnaryExpr(target, o, pos)
		default:
			return target
		}
		lhs = false
	}
}

func (p *parser) expression(lhs bool) ast.Expression {
	return p.postfixUnary(lhs)
}

func (p *parser) lhs() ast.Expression {
	return p.expression(true)
}

func (p *parser) rhs() ast.Expression {
	return p.expression(false)
}

func (p *parser) parse() *ast.CompilationUnit {
	p.openScope()
	unitScope := p.scope
	target := p.rhs()
	p.closeScope()
	assert(p.scope == nil, "unbalanced scopes")

	idx := 0
	for _, id := range p.unresolved {
		assert(id.Symbol == unresolved, "symbol already resolved")
		id.Symbol = unitScope.Find(id.Value)
		if id.Symbol == nil {
			p.unresolved[idx] = id
			idx++
		}
	}

	return &ast.CompilationUnit{
		Target:     target,
		Scope:      unitScope,
		Unresolved: p.unresolved[:idx],
	}
}

func Parse(s token.ITokenStream, fileName string, maxErrorsCount int) (unit *ast.CompilationUnit, err error) {
	if maxErrorsCount < 0 {
		maxErrorsCount = 0
	}
	p := &parser{
		ts:             s,
		tok:            token.Token{},
		el:             errors.NewErrorListWithCapacity(maxErrorsCount + 1),
		maxErrorsCount: maxErrorsCount,
	}

	defer func() {
		if e := recover(); e != nil {
			if ie, ok := e.(*internalError); ok {
				err = ie
			} else if _, ok := e.(stopParsing); !ok {
				// unknown error
				panic(e)
			}
		}

		if unit == nil {
			unit = &ast.CompilationUnit{
				Target: ast.CreateInvalidExpr("no expression", p.tok.Where),
				Scope:  nil,
			}
		}

		unit.Name = fileName

		err = p.el.Error()
	}()

	p.next(true)

	unit = p.parse()

	return
}
