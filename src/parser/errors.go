package parser

import "gitlab.com/rhinestone-project/go-rsbuild/src/errors"

// special panic struct for stop parse process
type stopParsing struct{}

// parser internal error
type internalError struct {
	message string
}

func (ie *internalError) Error() string {
	return ie.message
}

func newInternalError(msg string) error {
	return &internalError{message: errors.CodeInternalError.WithMessage(msg)}
}
