package parser

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/ast"
	"gitlab.com/rhinestone-project/go-rsbuild/src/errors"
	"gitlab.com/rhinestone-project/go-rsbuild/src/lexer"
	"gitlab.com/rhinestone-project/go-rsbuild/src/token"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"reflect"
	"testing"
)

func newLexer(source string) *lexer.Lexer {
	if d, err := util.ReadSourceData(source); err != nil {
		panic(err)
	} else if l, err := lexer.NewLexer(d); err != nil {
		panic(err)
	} else {
		return l
	}
}

func newParser(source string, maxErrorsCount int) *parser {
	p := &parser{
		ts:             newLexer(source),
		tok:            token.Token{},
		el:             errors.NewErrorListWithCapacity(maxErrorsCount + 1),
		maxErrorsCount: maxErrorsCount,
	}
	p.next(true)
	return p
}

func TestParse(t *testing.T) {}

func Test_assert(t *testing.T) {}

func Test_parser_error(t *testing.T) {}

func Test_parser_errorAt(t *testing.T) {}

func Test_parser_errorExpected(t *testing.T) {}

func Test_parser_errorExpectedAt(t *testing.T) {}

func Test_parser_errorUnexpected(t *testing.T) {}

func Test_parser_expect(t *testing.T) {}

func Test_parser_expression(t *testing.T) {
	t.Parallel()

	type args struct {
		lhs bool
	}
	tests := []struct {
		name   string
		source string
		args   args
		want   ast.Expression
	}{
		{
			name:   "lhs",
			source: "1234",
			args: args{
				lhs: true,
			},
			want: func() ast.Expression {
				if lit, err := ast.CreateLiteral("1234", token.TypeIntLiteral, util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 4,
						Row:    1,
						Col:    5,
					},
				}); err != nil {
					panic(err)
				} else {
					return lit
				}
			}(),
		},
		{
			name:   "rhs",
			source: "1234",
			args: args{
				lhs: false,
			},
			want: func() ast.Expression {
				if lit, err := ast.CreateLiteral("1234", token.TypeIntLiteral, util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 4,
						Row:    1,
						Col:    5,
					},
				}); err != nil {
					panic(err)
				} else {
					return lit
				}
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := newParser(tt.source, 0)
			if got := p.expression(tt.args.lhs); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("expression() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parser_handleError(t *testing.T) {}

func Test_parser_lhs(t *testing.T) {}

func Test_parser_next(t *testing.T) {}

func Test_parser_parse(t *testing.T) {}

func Test_parser_rhs(t *testing.T) {}

func Test_parser_simple(t *testing.T) {
	t.Parallel()

	type args struct {
		lhs bool
	}
	tests := []struct {
		name   string
		source string
		args   args
		want   ast.Expression
	}{
		{
			name:   "int",
			source: "1_2_3_4",
			args: args{
				lhs: true,
			},
			want: func() ast.Expression {
				if lit, err := ast.CreateLiteral("1234", token.TypeIntLiteral, util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 7,
						Row:    1,
						Col:    8,
					},
				}); err != nil {
					panic(err)
				} else {
					return lit
				}
			}(),
		},
		{
			name:   "string",
			source: "\"\\\"string\\\"\"",
			args: args{
				lhs: true,
			},
			want: func() ast.Expression {
				if lit, err := ast.CreateLiteral("\"\\\"string\\\"\"", token.TypeStringLiteral, util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 12,
						Row:    1,
						Col:    13,
					},
				}); err != nil {
					panic(err)
				} else {
					return lit
				}
			}(),
		},
		{
			name:   "id lhs",
			source: "id",
			args: args{
				lhs: true,
			},
			want: ast.CreateId("id", util.PositionRange{
				Begin: util.Position{
					Offset: 0,
					Row:    1,
					Col:    1,
				},
				End: util.Position{
					Offset: 2,
					Row:    1,
					Col:    3,
				},
			}),
		},
		{
			name:   "id rhs",
			source: "id",
			args: args{
				lhs: false,
			},
			want: func() ast.Expression {
				id := ast.CreateId("id", util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 2,
						Row:    1,
						Col:    3,
					},
				})
				id.Symbol = unresolved
				return id
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := newParser(tt.source, 0)
			if got := p.simple(tt.args.lhs); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("simple() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parser_postfixUnary(t *testing.T) {
	t.Parallel()

	type args struct {
		lhs bool
	}
	tests := []struct {
		name   string
		source string
		args   args
		want   ast.Expression
	}{
		{
			name:   "id postfix inc",
			source: "id ++",
			args: args{
				lhs: true,
			},
			want: func() ast.Expression {
				id := ast.CreateId("id", util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 2,
						Row:    1,
						Col:    3,
					},
				})
				id.Symbol = unresolved
				pos := util.Position{
					Offset: 3,
					Row:    1,
					Col:    4,
				}
				return ast.CreatePostfixUnaryExpr(id, token.TypePlus2, pos)
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := newParser(tt.source, 0)
			if got := p.postfixUnary(tt.args.lhs); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("postfixUnary() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_parser_id(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name   string
		source string
		want   ast.Expression
	}{
		{
			name:   "id",
			source: "id",
			want: func() ast.Expression {
				return ast.CreateId("id", util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 2,
						Row:    1,
						Col:    3,
					},
				})
			}(),
		},
		{
			name:   "not id",
			source: "2id",
			want: ast.CreateId("", util.PositionRange{
				Begin: util.Position{
					Offset: 0,
					Row:    1,
					Col:    1,
				},
				End: util.Position{
					Offset: 1,
					Row:    1,
					Col:    2,
				},
			}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := newParser(tt.source, 0)
			if got := p.id(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("id() = %v, want %v", got, tt.want)
			}
		})
	}
}
