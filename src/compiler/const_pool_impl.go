package compiler

import (
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"hash/fnv"
)

type constEntry struct {
	raw  []byte
	hash uint32
}

func hashOf(raw []byte, t rs_api.Type) (uint32, error) {
	h := fnv.New32()
	if _, err := h.Write(raw); err != nil {
		return 0, err
	}
	if _, err := h.Write([]byte{byte(t)}); err != nil {
		return 0, err
	}
	return h.Sum32(), nil
}

func fromBytes(raw []byte, t rs_api.Type) (constEntry, error) {
	hash, err := hashOf(raw, t)
	if err != nil {
		return constEntry{}, err
	}
	return constEntry{
		raw:  raw,
		hash: hash,
	}, nil
}

func newInt(i *rs_api.Int) (constEntry, error) {
	intBytes := i.ToBytes()
	bytes := make([]byte, 4+len(intBytes))
	l := len(intBytes)
	bytes[0] = byte((l >> 24) & 0xff)
	bytes[1] = byte((l >> 16) & 0xff)
	bytes[2] = byte((l >> 8) & 0xff)
	bytes[3] = byte(l & 0xff)
	copy(bytes[4:], intBytes)
	return fromBytes(bytes, rs_api.TypeInt)
}

func newString(s *rs_api.String) (constEntry, error) {
	stringBytes := []byte(s.String())
	bytes := make([]byte, 4+len(stringBytes))
	l := len(stringBytes)
	bytes[0] = byte((l >> 24) & 0xff)
	bytes[1] = byte((l >> 16) & 0xff)
	bytes[2] = byte((l >> 8) & 0xff)
	bytes[3] = byte(l & 0xff)
	copy(bytes[4:], stringBytes)
	return fromBytes(bytes, rs_api.TypeString)
}

type constPool struct {
	entries []constEntry
}

func (cp *constPool) push(entry constEntry) uint {
	offset := uint(0)
	for _, it := range cp.entries {
		if it.hash == entry.hash {
			return offset
		}
		offset += uint(len(it.raw))
	}
	cp.entries = append(cp.entries, entry)
	return offset
}

func (cp *constPool) PushInt(i *rs_api.Int) (uint, error) {
	entry, err := newInt(i)
	if err != nil {
		return 0, err
	}
	return cp.push(entry), nil
}

func (cp *constPool) PushString(s *rs_api.String) (uint, error) {
	entry, err := newString(s)
	if err != nil {
		return 0, err
	}
	return cp.push(entry), nil
}

func (cp *constPool) Data() []byte {
	size := 0
	for _, it := range cp.entries {
		size += len(it.raw)
	}
	result := make([]byte, size)
	offset := 0
	for _, it := range cp.entries {
		end := offset + len(it.raw)
		copy(result[offset:end], it.raw)
	}
	return result
}
