package compiler

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/ast"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
)

type compilerMessageType byte

const (
	messageError compilerMessageType = iota
	messageWarning
)

type compilerMessage struct {
	message string
	where   util.Position
	t       compilerMessageType
}

func (cm *compilerMessage) String() string {
	return cm.where.String() + " " + cm.message
}

type compilerMessages []compilerMessage

func (cm *compilerMessages) add(message string, where util.Position, t compilerMessageType) {
	*cm = append(*cm, compilerMessage{
		message: message,
		where:   where,
		t:       t,
	})
}

type compiler struct {
	warningIsError             bool
	errorsCount, warningsCount int
	messages                   compilerMessages
	cp                         constPool
}

func (c *compiler) WarningIsError() bool {
	return c.warningIsError
}

func (c *compiler) Error(message string, where util.Position) {
	c.errorsCount++
	c.messages.add(message, where, messageError)
}

func (c *compiler) filterMessages(t compilerMessageType) []string {
	result := make([]string, len(c.messages))
	idx := 0
	for _, msg := range c.messages {
		if msg.t == t {
			result[idx] = msg.String()
			idx++
		}
	}
	return result[:idx]
}

func (c *compiler) GetErrors() []string {
	return c.filterMessages(messageError)
}

func (c *compiler) GetErrorsCount() int {
	return c.errorsCount
}

func (c *compiler) Warning(message string, where util.Position) {
	t := messageWarning
	if c.warningIsError {
		t = messageError
		c.errorsCount++
	} else {
		c.warningsCount++
	}
	c.messages.add(message, where, t)
}

func (c *compiler) GetWarnings() []string {
	return c.filterMessages(messageWarning)
}

func (c *compiler) GetWarningsCount() int {
	return c.warningsCount
}

func (c *compiler) GetCompilerOptions() ast.CompilerOptions {
	return c
}

func (c *compiler) ConstPool() ast.ConstPool {
	return &c.cp
}

func NewCompiler(warningIsError bool) ast.CompilerEnv {
	return &compiler{
		warningIsError: warningIsError,
		errorsCount:    0,
		warningsCount:  0,
		messages:       make(compilerMessages, 0, 4),
		cp:             constPool{make([]constEntry, 0, 2)},
	}
}
