package compiler

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/ast"
	"gitlab.com/rhinestone-project/go-rsbuild/src/bytecode"
	"gitlab.com/rhinestone-project/go-rsbuild/src/lexer"
	"gitlab.com/rhinestone-project/go-rsbuild/src/parser"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	vmbytecode "gitlab.com/rhinestone-project/go-rsvm/src/bytecode"
	"gitlab.com/rhinestone-project/go-rsvm/src/env"
	"strings"
	"testing"
)

var source = `
tmpi++
`

func BenchmarkParseAndCompile(b *testing.B) {
	for i := 0; i < b.N; i++ {
		src, err := util.ReadSourceData(source)
		if err != nil {
			b.Error("source not loaded:", err.Error())
			continue
		}

		ts, err := lexer.NewLexer(src)
		if err != nil {
			b.Error("lexer not created:", err.Error())
			continue
		}

		unit, err := parser.Parse(ts, "<Unnamed>", 1)
		if err != nil {
			b.Error("parsing error:", err.Error())
			continue
		}

		globals := ast.NewScope(nil)
		tmpVar := ast.NewSymbol("tmpi", ast.SymbolTypeVar)
		tmpVar.Data = rs_api.TypeInt
		globals.Put(tmpVar)

		if !unit.Resolve(globals) {
			unresolved := make([]string, len(unit.Unresolved))
			for idx, it := range unit.Unresolved {
				unresolved[idx] = it.Value
			}
			b.Error("unresolved symbols:", strings.Join(unresolved, ", "))
			continue
		}

		compiler := NewCompiler(false)
		if !unit.Compile(compiler) {
			b.Error("compilation errors:", "  "+strings.Join(compiler.GetErrors(), "\n  "))
			continue
		}
	}
}

func compileAndRun(code string, t *testing.T) int {
	src, err := util.ReadSourceData(code)
	if err != nil {
		t.Error(err)
		return 1
	}

	ts, err := lexer.NewLexer(src)
	if err != nil {
		t.Error(err)
		return 2
	}

	unit, err := parser.Parse(ts, "<Unnamed>", 5)
	if err != nil {
		t.Error(err)
		return 3
	}

	globals := ast.NewScope(nil)
	tmp := ast.NewSymbol("tmpi", ast.SymbolTypeVar)
	tmp.Data = rs_api.TypeInt
	globals.Put(tmp)
	tmp = ast.NewSymbol("tmps", ast.SymbolTypeVar)
	tmp.Data = rs_api.TypeString
	globals.Put(tmp)

	if !unit.Resolve(globals) {
		unresolved := make([]string, len(unit.Unresolved))
		for idx, it := range unit.Unresolved {
			unresolved[idx] = it.Value
		}
		t.Error("unresolved symbols: " + strings.Join(unresolved, ", "))
		return 4
	}

	c := NewCompiler(false)
	if !unit.Compile(c) {
		t.Error("compilation errors: " + strings.Join(c.GetErrors(), "\n  "))
		return 5
	}

	out := bytecode.New()
	if !unit.Generate(c, out) {
		t.Error("generation errors: " + strings.Join(c.GetErrors(), "\n  "))
		return 6
	}
	out.End()

	cp := c.ConstPool().Data()
	cpl := len(cp)
	bc := out.Data()
	bcl := len(bc)
	bytes := make([]byte, 20+cpl+bcl)
	bytes[0] = 13
	bytes[1] = 12
	bytes[2] = 9
	bytes[3] = 18
	bytes[4] = 26
	bytes[5] = 7
	bytes[6] = 26
	bytes[7] = 28
	bytes[8] = 0
	bytes[9] = 0
	bytes[10] = 0
	bytes[11] = 0
	bytes[12] = byte((cpl >> 24) & 0xff)
	bytes[13] = byte((cpl >> 16) & 0xff)
	bytes[14] = byte((cpl >> 8) & 0xff)
	bytes[15] = byte(cpl & 0xff)
	copy(bytes[16:16+cpl], cp)
	bytes[16+cpl] = byte((bcl >> 24) & 0xff)
	bytes[17+cpl] = byte((bcl >> 16) & 0xff)
	bytes[18+cpl] = byte((bcl >> 8) & 0xff)
	bytes[19+cpl] = byte(bcl & 0xff)
	copy(bytes[20+cpl:], bc)

	p, err := vmbytecode.LoadProgram(bytes)
	if err != nil {
		t.Error(err)
		return 7
	}
	e := env.NewEnv()

	err = p.Execute(e, []string{})
	if err != nil {
		t.Error(err)
		return 8
	}

	exitCode := e.GetInt()
	if exitCode != nil {
		if exitCode.CompareTo(rs_api.Zero) != 0 {
			return exitCode.ToInt()
		}
	}

	return 0
}

func TestInc(t *testing.T) {
	exitCode := compileAndRun("tmpi--++--", t)
	if exitCode != -1 {
		t.Error("Unexpected exit code ", exitCode)
		return
	}
}
