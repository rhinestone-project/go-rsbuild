package token

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/errors"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"reflect"
	"testing"
)

type tokensArray struct {
	arr []Token
	pos int
}

func tokensOf(t ...Token) *tokensArray {
	return &tokensArray{
		arr: t,
		pos: 0,
	}
}

func (t *tokensArray) Next(errorHandler errors.ErrorHandler) (token Token) {
	switch {
	case t.pos < len(t.arr):
		token = t.arr[t.pos]
		t.pos++
	case t.pos == len(t.arr):
		token = Token{
			Text:  "",
			Type:  TypeEOF,
			Where: util.PositionRange{},
		}
		t.pos++
	default:
		errorHandler("not enough tokens", &util.Position{})
		token = Token{
			Text:  "Invalid",
			Type:  TypeInvalid,
			Where: util.PositionRange{},
		}
	}
	return
}

func TestTokenize(t *testing.T) {
	t.Parallel()

	nl := Token{
		Text:  "\n",
		Type:  TypeEOL,
		Where: util.PositionRange{},
	}
	il := Token{
		Text:  "0",
		Type:  TypeIntLiteral,
		Where: util.PositionRange{},
	}

	type args struct {
		ts ITokenStream
	}
	tests := []struct {
		name    string
		args    args
		want    []Token
		wantErr bool
	}{
		{
			name: "valid",
			args: args{
				ts: tokensOf(nl, il),
			},
			want: []Token{
				nl,
				il,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Tokenize(tt.args.ts)
			if (err != nil) != tt.wantErr {
				t.Errorf("Tokenize() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Tokenize() got = %v, want %v", got, tt.want)
			}
		})
	}
}
