package token

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"testing"
)

func TestToken_EscapedText(t1 *testing.T) {
	t1.Parallel()

	type fields struct {
		Text  string
		Type  Type
		Where util.PositionRange
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "normal",
			fields: fields{
				Text:  "it is text",
				Type:  TypeInvalid,
				Where: util.PositionRange{},
			},
			want: "it is text",
		},
		{
			name: "with new lines",
			fields: fields{
				Text:  "it\nis\ntext",
				Type:  TypeInvalid,
				Where: util.PositionRange{},
			},
			want: "it\\nis\\ntext",
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Token{
				Text:  tt.fields.Text,
				Type:  tt.fields.Type,
				Where: tt.fields.Where,
			}
			if got := t.EscapedText(); got != tt.want {
				t1.Errorf("EscapedText() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToken_IsEOF(t1 *testing.T) {
	t1.Parallel()

	type fields struct {
		Text  string
		Type  Type
		Where util.PositionRange
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "eof",
			fields: fields{
				Text:  "",
				Type:  TypeEOF,
				Where: util.PositionRange{},
			},
			want: true,
		},
		{
			name: "not eof",
			fields: fields{
				Text:  "0",
				Type:  TypeIntLiteral,
				Where: util.PositionRange{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Token{
				Text:  tt.fields.Text,
				Type:  tt.fields.Type,
				Where: tt.fields.Where,
			}
			if got := t.IsEOF(); got != tt.want {
				t1.Errorf("IsEOF() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToken_IsInvalid(t1 *testing.T) {
	t1.Parallel()

	type fields struct {
		Text  string
		Type  Type
		Where util.PositionRange
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "invalid",
			fields: fields{
				Text:  "",
				Type:  TypeInvalid,
				Where: util.PositionRange{},
			},
			want: true,
		},
		{
			name: "valid",
			fields: fields{
				Text:  "0",
				Type:  TypeIntLiteral,
				Where: util.PositionRange{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Token{
				Text:  tt.fields.Text,
				Type:  tt.fields.Type,
				Where: tt.fields.Where,
			}
			if got := t.IsInvalid(); got != tt.want {
				t1.Errorf("IsInvalid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToken_IsNewLine(t1 *testing.T) {
	t1.Parallel()

	type fields struct {
		Text  string
		Type  Type
		Where util.PositionRange
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "new line",
			fields: fields{
				Text:  "\n",
				Type:  TypeEOL,
				Where: util.PositionRange{},
			},
			want: true,
		},
		{
			name: "not new line",
			fields: fields{
				Text:  "0",
				Type:  TypeIntLiteral,
				Where: util.PositionRange{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Token{
				Text:  tt.fields.Text,
				Type:  tt.fields.Type,
				Where: tt.fields.Where,
			}
			if got := t.IsNewLine(); got != tt.want {
				t1.Errorf("IsNewLine() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToken_IsValid(t1 *testing.T) {
	t1.Parallel()

	type fields struct {
		Text  string
		Type  Type
		Where util.PositionRange
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "valid",
			fields: fields{
				Text:  "0",
				Type:  TypeIntLiteral,
				Where: util.PositionRange{},
			},
			want: true,
		},
		{
			name: "invalid",
			fields: fields{
				Text:  "",
				Type:  TypeInvalid,
				Where: util.PositionRange{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Token{
				Text:  tt.fields.Text,
				Type:  tt.fields.Type,
				Where: tt.fields.Where,
			}
			if got := t.IsValid(); got != tt.want {
				t1.Errorf("IsValid() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToken_IsWS(t1 *testing.T) {
	t1.Parallel()

	type fields struct {
		Text  string
		Type  Type
		Where util.PositionRange
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "whitespace",
			fields: fields{
				Text:  "\t ",
				Type:  TypeWhiteSpace,
				Where: util.PositionRange{},
			},
			want: true,
		},
		{
			name: "not whitespace",
			fields: fields{
				Text:  "\n",
				Type:  TypeEOL,
				Where: util.PositionRange{},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Token{
				Text:  tt.fields.Text,
				Type:  tt.fields.Type,
				Where: tt.fields.Where,
			}
			if got := t.IsWS(); got != tt.want {
				t1.Errorf("IsWS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestToken_String(t1 *testing.T) {
	t1.Parallel()

	type fields struct {
		Text  string
		Type  Type
		Where util.PositionRange
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "int literal",
			fields: fields{
				Text: "1234",
				Type: TypeIntLiteral,
				Where: util.PositionRange{
					Begin: util.Position{
						Offset: 0,
						Row:    1,
						Col:    1,
					},
					End: util.Position{
						Offset: 0,
						Row:    1,
						Col:    5,
					},
				},
			},
			want: "[1:1]-[1:5] (IntLiteral): \"1234\"",
		},
	}
	for _, tt := range tests {
		t1.Run(tt.name, func(t1 *testing.T) {
			t := &Token{
				Text:  tt.fields.Text,
				Type:  tt.fields.Type,
				Where: tt.fields.Where,
			}
			if got := t.String(); got != tt.want {
				t1.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}
