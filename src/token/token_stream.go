package token

import "gitlab.com/rhinestone-project/go-rsbuild/src/errors"

type ITokenStream interface {
	Next(errorHandler errors.ErrorHandler) Token
}
