package token

import "testing"

func TestType_IsEOF(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want bool
	}{
		{
			name: "is eof",
			t:    TypeEOF,
			want: true,
		},
		{
			name: "is not eof",
			t:    TypeEOL,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.IsEOF(); got != tt.want {
				t.Errorf("IsEOF() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestType_IsEOL(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want bool
	}{
		{
			name: "is eol",
			t:    TypeEOL,
			want: true,
		},
		{
			name: "is not eol",
			t:    TypeEOF,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.IsEOL(); got != tt.want {
				t.Errorf("IsEOL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestType_IsLiteral(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want bool
	}{
		{
			name: "int literal",
			t:    TypeIntLiteral,
			want: true,
		},
		{
			name: "string literal",
			t:    TypeStringLiteral,
			want: true,
		},
		{
			name: "eof",
			t:    TypeEOF,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.IsLiteral(); got != tt.want {
				t.Errorf("IsLiteral() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestType_IsWhiteSpace(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want bool
	}{
		{
			name: "is whitespace",
			t:    TypeWhiteSpace,
			want: true,
		},
		{
			name: "is not whitespace",
			t:    TypeEOL,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.IsWhiteSpace(); got != tt.want {
				t.Errorf("IsWhiteSpace() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestType_IsSeparator(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want bool
	}{
		{
			name: "is separator",
			t:    TypeLParen,
			want: true,
		},
		{
			name: "is not separator",
			t:    TypeEOL,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.IsSeparator(); got != tt.want {
				t.Errorf("IsSeparator() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestType_IsOperator(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want bool
	}{
		{
			name: "is operator",
			t:    TypePlus,
			want: true,
		},
		{
			name: "is not operator",
			t:    TypeEOL,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.IsOperator(); got != tt.want {
				t.Errorf("IsOperator() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestType_Name(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want string
	}{
		{
			name: "invalid",
			t:    TypeInvalid,
			want: "Invalid",
		},
		{
			name: "int literal",
			t:    TypeIntLiteral,
			want: "IntLiteral",
		},
		{
			name: "string literal",
			t:    TypeStringLiteral,
			want: "StringLiteral",
		},
		{
			name: "eol",
			t:    TypeEOL,
			want: "EOL",
		},
		{
			name: "whitespace",
			t:    TypeWhiteSpace,
			want: "WhiteSpace",
		},
		{
			name: "id",
			t:    TypeIdentifier,
			want: "Identifier",
		},
		{
			name: "left paren",
			t:    TypeLParen,
			want: "LParen",
		},
		{
			name: "right paren",
			t:    TypeRParen,
			want: "RParen",
		},
		{
			name: "plus",
			t:    TypePlus,
			want: "Plus",
		},
		{
			name: "plus plus",
			t:    TypePlus2,
			want: "Plus2",
		},
		{
			name: "minus",
			t:    TypeMinus,
			want: "Minus",
		},
		{
			name: "minus minus",
			t:    TypeMinus2,
			want: "Minus2",
		},
		{
			name: "eof",
			t:    TypeEOF,
			want: "EOF",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.Name(); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestType_Repr(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want string
	}{
		{
			name: "invalid",
			t:    TypeInvalid,
			want: "Invalid",
		},
		{
			name: "int literal",
			t:    TypeIntLiteral,
			want: "[0-9]+",
		},
		{
			name: "string literal",
			t:    TypeStringLiteral,
			want: "'\"' ~[\\n\\r] '\"' | '\"\"\"' {Any} '\"\"\"'",
		},
		{
			name: "eol",
			t:    TypeEOL,
			want: "'\\n' | '\\r' | '\\r\\n'",
		},
		{
			name: "whitespace",
			t:    TypeWhiteSpace,
			want: "' ' | '\\t'",
		},
		{
			name: "id",
			t:    TypeIdentifier,
			want: "('_' | {Letter}) ('_' | {Letter} | {Digit})*",
		},
		{
			name: "left paren",
			t:    TypeLParen,
			want: "(",
		},
		{
			name: "right paren",
			t:    TypeRParen,
			want: ")",
		},
		{
			name: "plus",
			t:    TypePlus,
			want: "+",
		},
		{
			name: "plus plus",
			t:    TypePlus2,
			want: "++",
		},
		{
			name: "minus",
			t:    TypeMinus,
			want: "-",
		},
		{
			name: "minus minus",
			t:    TypeMinus2,
			want: "--",
		},
		{
			name: "eof",
			t:    TypeEOF,
			want: "'\\0'",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.Repr(); got != tt.want {
				t.Errorf("Repr() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestType_String(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		t    Type
		want string
	}{
		{
			name: "check",
			t:    TypeIntLiteral,
			want: "IntLiteral",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}
