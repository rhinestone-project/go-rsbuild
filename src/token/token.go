package token

import "gitlab.com/rhinestone-project/go-rsbuild/src/util"

type Token struct {
	Text  string
	Type  Type
	Where util.PositionRange
}

func (t *Token) EscapedText() string {
	return util.EscapedStringOfString(t.Text)
}

func (t *Token) String() string {
	val := t.EscapedText()
	if len(val) >= 77 {
		val = val[:77] + "\"..."
	} else {
		val += "\""
	}
	return t.Where.String() + " (" + t.Type.Name() + "): \"" + val
}

func (t *Token) IsEOF() bool {
	return t.Type.IsEOF()
}

func (t *Token) IsInvalid() bool {
	return t.Type == TypeInvalid
}

func (t *Token) IsValid() bool {
	return t.Type != TypeInvalid
}

func (t *Token) IsNewLine() bool {
	return t.Type.IsEOL()
}

func (t *Token) IsWS() bool {
	return t.Type.IsWhiteSpace()
}
