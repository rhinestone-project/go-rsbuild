package token

type Type byte

const (
	_ Type = iota

	TypeInvalid // Error or same

	literalsBegin
	TypeIntLiteral    // [0-9]+
	TypeStringLiteral // '"' ~[\n\r] '"' | '"""' {Any} '"""'
	literalsEnd

	eolBegin
	TypeEOL // '\n' | '\r' | '\r\n'
	eolEnd

	whitespacesBegin
	TypeWhiteSpace // ' ' | '\t'
	whitespacesEnd

	TypeIdentifier // ('_' | {Letter}) ('_' | {Letter} | {Digit})*

	separatorsBegin
	TypeLParen // '('
	TypeRParen // ')'
	separatorsEnd

	operatorsBegin
	TypePlus   // '+'
	TypePlus2  // '++'
	TypeMinus  // '-'
	TypeMinus2 // '--'
	operatorsEnd

	TypeEOF
)

var ttNames = [...]string{
	TypeInvalid:       "Invalid",
	TypeIntLiteral:    "IntLiteral",
	TypeStringLiteral: "StringLiteral",
	TypeEOL:           "EOL",
	TypeWhiteSpace:    "WhiteSpace",
	TypeIdentifier:    "Identifier",
	TypeLParen:        "LParen",
	TypeRParen:        "RParen",
	TypePlus:          "Plus",
	TypePlus2:         "Plus2",
	TypeMinus:         "Minus",
	TypeMinus2:        "Minus2",
	TypeEOF:           "EOF",
}

func (t Type) Name() string {
	return ttNames[t]
}

var ttRepr = [...]string{
	TypeInvalid:       "Invalid",
	TypeIntLiteral:    "[0-9]+",
	TypeStringLiteral: "'\"' ~[\\n\\r] '\"' | '\"\"\"' {Any} '\"\"\"'",
	TypeEOL:           "'\\n' | '\\r' | '\\r\\n'",
	TypeWhiteSpace:    "' ' | '\\t'",
	TypeIdentifier:    "('_' | {Letter}) ('_' | {Letter} | {Digit})*",
	TypeLParen:        "(",
	TypeRParen:        ")",
	TypePlus:          "+",
	TypePlus2:         "++",
	TypeMinus:         "-",
	TypeMinus2:        "--",
	TypeEOF:           "'\\0'",
}

func (t Type) Repr() string {
	return ttRepr[t]
}

func (t Type) String() string {
	return t.Name()
}

func (t Type) IsLiteral() bool {
	return literalsBegin < t && t < literalsEnd
}

func (t Type) IsEOL() bool {
	return eolBegin < t && t < eolEnd
}

func (t Type) IsWhiteSpace() bool {
	return whitespacesBegin < t && t < whitespacesEnd
}

func (t Type) IsSeparator() bool {
	return separatorsBegin < t && t < separatorsEnd
}

func (t Type) IsOperator() bool {
	return operatorsBegin < t && t < operatorsEnd
}

func (t Type) IsEOF() bool {
	return t == TypeEOF
}
