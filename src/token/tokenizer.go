package token

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/errors"
)

func Tokenize(ts ITokenStream) ([]Token, error) {
	el := errors.ErrorList{}
	result := make([]Token, 0, 4)

	for {
		t := ts.Next(el.Add)
		if t.IsEOF() || t.IsInvalid() {
			break
		}
		result = append(result, t)
	}

	el.Sort()
	return result, el.Error()
}
