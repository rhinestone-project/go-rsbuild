package errors

import (
	"bytes"
	"errors"
	"fmt"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"sort"
)

type ErrorPosition interface {
	util.Comparable
	fmt.Stringer
	Line() int
}

type ErrorHandler func(message string, where ErrorPosition)

type errorEntry struct {
	message  string
	position ErrorPosition
}

func (ee *errorEntry) Line() int {
	return ee.position.Line()
}

type ErrorList []errorEntry

func NewErrorList() ErrorList {
	return make(ErrorList, 0, 2)
}

func NewErrorListWithCapacity(capacity int) ErrorList {
	return make(ErrorList, 0, capacity)
}

func (el *ErrorList) Add(message string, where ErrorPosition) {
	*el = append(*el, errorEntry{
		message:  message,
		position: where,
	})
}

func (el *ErrorList) Len() int {
	return len(*el)
}

func (el *ErrorList) Less(i, j int) bool {
	a := (*el)[i]
	b := (*el)[j]
	if a.position == nil {
		return true
	} else if b.position == nil {
		return false
	}
	return util.Compare(a.position, b.position).Less()
}

func (el *ErrorList) Swap(i, j int) {
	(*el)[i], (*el)[j] = (*el)[j], (*el)[i]
}

func (el *ErrorList) Sort() {
	sort.Sort(el)
}

func (el *ErrorList) Error() error {
	if el.Len() == 0 {
		return nil
	}
	buf := &bytes.Buffer{}
	for idx, it := range *el {
		if idx > 0 {
			buf.WriteByte('\n')
		}
		if it.position != nil {
			buf.WriteString("  at " + it.position.String() + " \"")
		}
		buf.WriteString(it.message)
		if it.position != nil {
			buf.WriteString("\"")
		}
	}
	return errors.New(buf.String())
}

func (el *ErrorList) Last() string {
	if el.Len() == 0 {
		return ""
	}
	return (*el)[el.Len()-1].message
}
