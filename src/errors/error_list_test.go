package errors

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"reflect"
	"strconv"
	"testing"
)

type testPos struct {
	int
}

func (pos *testPos) CompareTo(other util.Comparable) int {
	if o, ok := other.(*testPos); ok {
		return pos.int - o.int
	}
	panic(util.CannotCompareError(pos, other))
}

func (pos *testPos) Line() int {
	return pos.int
}

func (pos *testPos) String() string {
	return strconv.Itoa(pos.int)
}

func testPosOf(pos int) *testPos {
	return &testPos{pos}
}

func TestErrorList_Add(t *testing.T) {
	t.Parallel()

	type args struct {
		message string
		where   ErrorPosition
	}
	tests := []struct {
		name string
		el   ErrorList
		args args
		want ErrorList
	}{
		{
			name: "add error",
			el:   make(ErrorList, 0, 1),
			args: args{
				message: "it's error",
				where:   testPosOf(1),
			},
			want: ErrorList{
				errorEntry{
					message:  "it's error",
					position: testPosOf(1),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.el.Add(tt.args.message, tt.args.where)
			if !reflect.DeepEqual(tt.el, tt.want) {
				t.Errorf("Add() = %v, want %v", tt.el, tt.want)
			}
		})
	}
}

func TestErrorList_Error(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		el      ErrorList
		wantErr bool
	}{
		{
			name:    "empty list",
			el:      ErrorList{},
			wantErr: false,
		},
		{
			name: "not empty list",
			el: ErrorList{
				errorEntry{
					message:  "it's error",
					position: testPosOf(1),
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.el.Error(); (err != nil) != tt.wantErr {
				t.Errorf("Error() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestErrorList_Len(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		el   ErrorList
		want int
	}{
		{
			name: "empty list",
			el:   ErrorList{},
			want: 0,
		},
		{
			name: "not empty list",
			el: ErrorList{
				errorEntry{
					message:  "it's error",
					position: testPosOf(1),
				},
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.el.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestErrorList_Less(t *testing.T) {
	t.Parallel()

	type args struct {
		i int
		j int
	}
	tests := []struct {
		name string
		el   ErrorList
		args args
		want bool
	}{
		{
			name: "unsorted",
			el: ErrorList{
				errorEntry{
					message:  "error 1",
					position: testPosOf(2),
				},
				errorEntry{
					message:  "error 2",
					position: testPosOf(1),
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: false,
		},
		{
			name: "sorted",
			el: ErrorList{
				errorEntry{
					message:  "error 1",
					position: testPosOf(1),
				},
				errorEntry{
					message:  "error 2",
					position: testPosOf(2),
				},
			},
			args: args{
				i: 0,
				j: 1,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.el.Less(tt.args.i, tt.args.j); got != tt.want {
				t.Errorf("Less() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestErrorList_Sort(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		el   ErrorList
		want ErrorList
	}{
		{
			name: "unsorted",
			el: ErrorList{
				errorEntry{
					message:  "error 1",
					position: testPosOf(2),
				},
				errorEntry{
					message:  "error 2",
					position: testPosOf(1),
				},
			},
			want: ErrorList{
				errorEntry{
					message:  "error 2",
					position: testPosOf(1),
				},
				errorEntry{
					message:  "error 1",
					position: testPosOf(2),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.el.Sort()
			if !reflect.DeepEqual(tt.el, tt.want) {
				t.Errorf("Sort() = %v, want %v", tt.el, tt.want)
			}
		})
	}
}

func TestErrorList_Swap(t *testing.T) {
	t.Parallel()

	type args struct {
		i int
		j int
	}
	tests := []struct {
		name string
		el   ErrorList
		args args
		want ErrorList
	}{
		{
			name: "swap [0] and [2]",
			el: ErrorList{
				errorEntry{
					message:  "error 1",
					position: testPosOf(3),
				},
				errorEntry{
					message:  "error 2",
					position: testPosOf(2),
				},
				errorEntry{
					message:  "error 3",
					position: testPosOf(1),
				},
			},
			args: args{
				i: 0,
				j: 2,
			},
			want: ErrorList{
				errorEntry{
					message:  "error 3",
					position: testPosOf(1),
				},
				errorEntry{
					message:  "error 2",
					position: testPosOf(2),
				},
				errorEntry{
					message:  "error 1",
					position: testPosOf(3),
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.el.Swap(tt.args.i, tt.args.j)
			if !reflect.DeepEqual(tt.el, tt.want) {
				t.Errorf("Swap() = %v, want %v", tt.el, tt.want)
			}
		})
	}
}

func TestNewErrorList(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		want ErrorList
	}{
		{
			name: "new empty list",
			want: make(ErrorList, 0, 2),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewErrorList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewErrorList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewErrorListWithCapacity(t *testing.T) {
	t.Parallel()

	type args struct {
		capacity int
	}
	tests := []struct {
		name string
		args args
		want ErrorList
	}{
		{
			name: "empty list with capacity",
			args: args{
				capacity: 5,
			},
			want: make(ErrorList, 0, 5),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewErrorListWithCapacity(tt.args.capacity); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewErrorListWithCapacity() = %v, want %v", got, tt.want)
			}
		})
	}
}
