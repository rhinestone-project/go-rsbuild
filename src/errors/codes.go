package errors

import (
	"strconv"
	"strings"
)

type Code uint

const (
	CodeUnknown Code = iota
	CodeInternalError
	CodeUnexpectedEOF
	CodeUnexpectedEOL
)

var codeName = [...]string{
	CodeUnknown:       "Unknown",
	CodeInternalError: "InternalError",
	CodeUnexpectedEOF: "UnexpectedEOF",
	CodeUnexpectedEOL: "UnexpectedEOL",
}

func (c *Code) Name() string {
	return codeName[*c]
}

func (c Code) Number() uint {
	return uint(c)
}

func (c Code) NumberString() string {
	s := strconv.FormatInt(int64(c.Number()), 16)
	if len(s) < 4 {
		s = strings.Repeat("0", 4-len(s)) + s
	}
	return s
}

func (c Code) String() string {
	return "C" + c.NumberString() + " (" + c.Name() + ")"
}

func (c Code) WithMessage(message string) string {
	return c.String() + ": " + message
}
