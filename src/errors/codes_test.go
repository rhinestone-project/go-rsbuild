package errors

import "testing"

func TestCode_Name(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		c    Code
		want string
	}{
		{
			name: "unknown",
			c:    CodeUnknown,
			want: "Unknown",
		},
		{
			name: "internal",
			c:    CodeInternalError,
			want: "InternalError",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.Name(); got != tt.want {
				t.Errorf("Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCode_Number(t *testing.T) {}

func TestCode_NumberString(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		c    Code
		want string
	}{
		{
			name: "unknown",
			c:    CodeUnknown,
			want: "0000",
		},
		{
			name: "internal",
			c:    CodeInternalError,
			want: "0001",
		},
		{
			name: "eof",
			c:    CodeUnexpectedEOF,
			want: "0002",
		},
		{
			name: "eol",
			c:    CodeUnexpectedEOL,
			want: "0003",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.NumberString(); got != tt.want {
				t.Errorf("NumberString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCode_String(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		c    Code
		want string
	}{
		{
			name: "unknown",
			c:    CodeUnknown,
			want: "C0000 (Unknown)",
		},
		{
			name: "internal",
			c:    CodeInternalError,
			want: "C0001 (InternalError)",
		},
		{
			name: "eof",
			c:    CodeUnexpectedEOF,
			want: "C0002 (UnexpectedEOF)",
		},
		{
			name: "eol",
			c:    CodeUnexpectedEOL,
			want: "C0003 (UnexpectedEOL)",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCode_WithMessage(t *testing.T) {
	t.Parallel()

	type args struct {
		message string
	}
	tests := []struct {
		name string
		c    Code
		args args
		want string
	}{
		{
			name: "unknown",
			c:    CodeUnknown,
			args: args{
				message: "\"\"",
			},
			want: "C0000 (Unknown): \"\"",
		},
		{
			name: "internal",
			c:    CodeInternalError,
			args: args{
				message: "\"\"",
			},
			want: "C0001 (InternalError): \"\"",
		},
		{
			name: "eof",
			c:    CodeUnexpectedEOF,
			args: args{
				message: "\"\"",
			},
			want: "C0002 (UnexpectedEOF): \"\"",
		},
		{
			name: "eol",
			c:    CodeUnexpectedEOL,
			args: args{
				message: "\"\"",
			},
			want: "C0003 (UnexpectedEOL): \"\"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.WithMessage(tt.args.message); got != tt.want {
				t.Errorf("WithMessage() = %v, want %v", got, tt.want)
			}
		})
	}
}
