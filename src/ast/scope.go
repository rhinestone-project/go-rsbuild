package ast

type Scope struct {
	Parent  *Scope
	Symbols map[string]*Symbol
}

func NewScope(parent *Scope) *Scope {
	return &Scope{
		Parent:  parent,
		Symbols: make(map[string]*Symbol),
	}
}

func (s *Scope) Find(symbolName string) *Symbol {
	return s.Symbols[symbolName]
}

func (s *Scope) Put(symbol *Symbol) (oldSymbol *Symbol) {
	if oldSymbol = s.Symbols[symbol.Name]; oldSymbol == nil {
		s.Symbols[symbol.Name] = symbol
	}
	return
}
