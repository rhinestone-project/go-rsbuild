package ast

type Visitable interface {
	Apply(v IVisitor) error
}

type IVisitor interface {
	VisitInvalidExpr(e *InvalidExpr) error
	VisitLiteralExpr(e *LiteralExpr) error
	VisitIdExpr(e *IdExpr) error
	VisitParenExpr(e *ParenExpr) error
	VisitPostfixUnaryExpr(e *PostfixUnaryExpr) error
}
