package ast

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
)

type CompilerOptions interface {
	WarningIsError() bool
}

type ConstPool interface {
	PushInt(*rs_api.Int) (uint, error)
	PushString(*rs_api.String) (uint, error)
	Data() []byte
}

type CompilerEnv interface {
	Error(message string, where util.Position)
	GetErrors() []string
	GetErrorsCount() int

	Warning(message string, where util.Position)
	GetWarnings() []string
	GetWarningsCount() int

	GetCompilerOptions() CompilerOptions

	ConstPool() ConstPool
}

type ByteCode interface {
	Data() []byte
	LoadConstInt(index uint)
	LoadConstString(index uint)
	LoadInt(name string)
	LoadString(name string)
	StoreInt(name string)
	StoreString(name string)
	PopInt()
	PopString()
	DubInt()
	DubString()
	InvokeNative(index rs_api.Index)
	End()
}
