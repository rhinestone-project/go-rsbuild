package ast

import (
	"fmt"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
)

type (
	INode interface {
		Pos() util.PositionRange
		Compile(env CompilerEnv)
		GetType() rs_api.Type
		Generate(env CompilerEnv, byteCode ByteCode) bool
	}

	Node struct {
		where util.PositionRange
	}

	Expression interface {
		INode
		fmt.Stringer
		iExpr()
	}
)

func (node *Node) Pos() util.PositionRange {
	return node.where
}
