package ast

import "io"

type basicPrinter struct {
	out io.Writer
}

func (b *basicPrinter) print(text string) error {
	_, err := b.out.Write([]byte(text))
	return err
}

func (b *basicPrinter) VisitInvalidExpr(e *InvalidExpr) error {
	return b.print(e.String())
}

func (b *basicPrinter) VisitLiteralExpr(e *LiteralExpr) error {
	return b.print(e.String())
}

func (b *basicPrinter) VisitIdExpr(e *IdExpr) error {
	return b.print(e.Value)
}

func (b *basicPrinter) VisitParenExpr(e *ParenExpr) error {
	if err := b.print("("); err != nil {
		return err
	}
	if err := e.Apply(b); err != nil {
		return err
	}
	return b.print(")")
}

func (b *basicPrinter) VisitPostfixUnaryExpr(e *PostfixUnaryExpr) error {
	if err := e.Apply(b); err != nil {
		return err
	}
	return b.print(" " + e.Operator.Repr() + " ")
}

func BasicPrint(v Visitable, out io.Writer) error {
	printer := &basicPrinter{out: out}
	return v.Apply(printer)
}
