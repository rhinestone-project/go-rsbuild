package ast

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
)

type IdExpr struct {
	Node
	Value  string
	Type   rs_api.Type
	Symbol *Symbol
}

func CreateId(text string, where util.PositionRange) *IdExpr {
	return &IdExpr{
		Node: Node{
			where: where,
		},
		Value:  text,
		Type:   rs_api.TypeVoid,
		Symbol: nil,
	}
}

func (id *IdExpr) String() string {
	return "<id \"" + id.Value + "\">"
}

func (id *IdExpr) Apply(v IVisitor) error {
	return v.VisitIdExpr(id)
}

func (id *IdExpr) iExpr() {}

func (id *IdExpr) Compile(env CompilerEnv) {
	if id.Type == rs_api.TypeVoid {
		if id.Symbol == nil {
			env.Error("unresolved symbol "+id.Value, id.where.Begin)
			return
		}
		id.Type = id.Symbol.Data.(rs_api.Type)
	}
	if id.Type == rs_api.TypeVoid {
		env.Error("identifier "+id.Value+" with void type", id.where.Begin)
	}
}

func (id *IdExpr) GetType() rs_api.Type {
	return id.Type
}

func (id *IdExpr) Generate(env CompilerEnv, byteCode ByteCode) bool {
	switch id.Type {
	case rs_api.TypeInt:
		byteCode.LoadInt(id.Value)
	case rs_api.TypeString:
		byteCode.LoadString(id.Value)
	default:
		env.Error("no load opcode for type "+id.Type.Name(), id.where.Begin)
		return false
	}
	return true
}
