package ast

import "gitlab.com/rhinestone-project/go-rsvm/rs_api"

type CompilationUnit struct {
	Name       string
	Target     Expression
	Scope      *Scope
	Unresolved []*IdExpr
}

func (unit *CompilationUnit) Resolve(globals *Scope) bool {
	rt := rs_api.RT
	idx := 0
	for _, id := range unit.Unresolved {
		if s := globals.Find(id.Value); s != nil {
			id.Symbol = s
		} else if t := rt.FindType(id.Value); t != nil {
			id.Symbol = NewSymbol(t.Name, SymbolTypeNativeType)
			id.Symbol.Data = t.Type
		} else {
			unit.Unresolved[idx] = id
			idx++
		}
	}
	unit.Unresolved = unit.Unresolved[:idx]
	return len(unit.Unresolved) == 0
}

func (unit *CompilationUnit) Compile(env CompilerEnv) bool {
	errorsCount := env.GetErrorsCount()
	unit.Target.Compile(env)
	return errorsCount == env.GetErrorsCount()
}

func (unit *CompilationUnit) Generate(env CompilerEnv, byteCode ByteCode) bool {
	errorsCount := env.GetErrorsCount()
	if !unit.Target.Generate(env, byteCode) {
		return false
	}
	return errorsCount == env.GetErrorsCount()
}
