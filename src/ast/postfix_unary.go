package ast

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/token"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
)

type PostfixUnaryExpr struct {
	Node
	Target        Expression
	Operator      token.Type
	OperatorPos   util.Position
	Type          rs_api.Type
	functionIndex rs_api.Index
}

func CreatePostfixUnaryExpr(target Expression,
	operator token.Type, operatorPos util.Position) *PostfixUnaryExpr {
	begin := target.Pos().Begin
	return &PostfixUnaryExpr{
		Node: Node{
			where: begin.RangeTo(operatorPos),
		},
		Target:      target,
		Operator:    operator,
		OperatorPos: operatorPos,
		Type:        rs_api.TypeVoid,
	}
}

func (un *PostfixUnaryExpr) String() string {
	return "<unary (" + un.Target.String() + ") " + un.Operator.Repr() + ">"
}

func (un *PostfixUnaryExpr) Apply(v IVisitor) error {
	return v.VisitPostfixUnaryExpr(un)
}

func (un *PostfixUnaryExpr) iExpr() {}

func (un *PostfixUnaryExpr) getOperatorFunctionName() string {
	var name string
	switch un.Operator {
	case token.TypePlus2:
		name = "inc"
	case token.TypeMinus2:
		name = "dec"
	default:
		return ""
	}
	return "<operator_" + name + ">"
}

func (un *PostfixUnaryExpr) findUnaryFunction(name string) {
	switch un.Target.GetType() {
	case rs_api.TypeInt, rs_api.TypeString:
		functions := rs_api.RT.FindFuncs(name, []rs_api.Type{un.Target.GetType()})
		for _, function := range functions {
			if len(function.ReturnTypes) == 1 {
				un.Type = function.ReturnTypes[0]
				un.functionIndex = function.Index()
				break
			}
		}
	}
}

func (un *PostfixUnaryExpr) Compile(env CompilerEnv) {
	un.Target.Compile(env)
	name := un.getOperatorFunctionName()
	if name == "" {
		env.Error("unsupported postfix operator '"+
			un.Operator.Repr()+"'", un.OperatorPos)
		return
	}
	un.findUnaryFunction(name)
	if un.Type == rs_api.TypeVoid {
		env.Error("postfix operator with void return type '"+
			un.Operator.Repr()+"'", un.OperatorPos)
	}
}

func (un *PostfixUnaryExpr) GetType() rs_api.Type {
	return un.Type
}

func (un *PostfixUnaryExpr) Generate(env CompilerEnv, byteCode ByteCode) bool {
	if !un.Target.Generate(env, byteCode) {
		return false
	}
	byteCode.InvokeNative(un.functionIndex)
	return true
}
