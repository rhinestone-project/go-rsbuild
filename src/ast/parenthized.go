package ast

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
)

type ParenExpr struct {
	Node
	LParen util.Position
	Target Expression
	RParen util.Position
	Type   rs_api.Type
}

func CreateParenExpr(lParenPos, rParenPos util.Position, target Expression) *ParenExpr {
	return &ParenExpr{
		Node: Node{
			where: lParenPos.RangeTo(rParenPos),
		},
		LParen: lParenPos,
		Target: target,
		RParen: rParenPos,
		Type:   rs_api.TypeVoid,
	}
}

func (p *ParenExpr) String() string {
	return "<paren (" + p.Target.String() + ")>"
}

func (p *ParenExpr) Apply(v IVisitor) error {
	return v.VisitParenExpr(p)
}

func (p *ParenExpr) iExpr() {}

func (p *ParenExpr) Compile(env CompilerEnv) {
	p.Target.Compile(env)
	p.Type = p.Target.GetType()
}

func (p *ParenExpr) GetType() rs_api.Type {
	return p.Type
}

func (p *ParenExpr) Generate(env CompilerEnv, byteCode ByteCode) bool {
	return p.Target.Generate(env, byteCode)
}
