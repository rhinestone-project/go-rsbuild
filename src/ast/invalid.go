package ast

import (
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
)

type (
	InvalidExpr struct {
		Node
		Message string
	}
)

func CreateInvalidExpr(message string, where util.PositionRange) *InvalidExpr {
	expr := &InvalidExpr{
		Node: Node{
			where: where,
		},
		Message: message,
	}
	return expr
}

func (inv *InvalidExpr) String() string {
	return "<invalid expr: \"" + inv.Message + "\">"
}

func (inv *InvalidExpr) Apply(v IVisitor) error {
	return v.VisitInvalidExpr(inv)
}

func (inv *InvalidExpr) iExpr() {}

func (inv *InvalidExpr) Compile(env CompilerEnv) {
	env.Error(inv.Message, inv.where.Begin)
}

func (inv *InvalidExpr) GetType() rs_api.Type {
	return rs_api.TypeVoid
}

func (inv *InvalidExpr) Generate(env CompilerEnv, _ ByteCode) bool {
	env.Error(inv.Message, inv.where.Begin)
	return false
}
