package ast

import (
	"errors"
	"gitlab.com/rhinestone-project/go-rsbuild/src/token"
	"gitlab.com/rhinestone-project/go-rsbuild/src/util"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
)

type LiteralExpr struct {
	Node
	IntValue    *rs_api.Int
	StringValue *rs_api.String
	Type        rs_api.Type
}

func CreateLiteral(text string, t token.Type, where util.PositionRange) (*LiteralExpr, error) {
	lit := &LiteralExpr{
		Node: Node{
			where: where,
		},
		IntValue: nil,
		Type:     rs_api.TypeVoid,
	}
	switch t {
	case token.TypeIntLiteral:
		if i, err := rs_api.NewInt(text, 10); err != nil {
			return nil, err
		} else {
			lit.Type = rs_api.TypeInt
			lit.IntValue = i
		}
	case token.TypeStringLiteral:
		if s, err := rs_api.NewString(text[1:len(text)-1], true); err != nil {
			return nil, err
		} else {
			lit.Type = rs_api.TypeString
			lit.StringValue = s
		}
	default:
		return nil, errors.New("unsupported literal type: " + t.Name())
	}
	return lit, nil
}

func (lit *LiteralExpr) String() string {
	switch lit.Type {
	case rs_api.TypeVoid:
		return "void()"
	case rs_api.TypeInt:
		return "int(" + lit.IntValue.String() + ")"
	case rs_api.TypeString:
		return "string(" + lit.StringValue.String() + ")"
	default:
		return "<unknown>"
	}
}

func (lit *LiteralExpr) Apply(v IVisitor) error {
	return v.VisitLiteralExpr(lit)
}

func (lit *LiteralExpr) iExpr() {}

func (lit *LiteralExpr) Compile(env CompilerEnv) {
	if lit.Type == rs_api.TypeVoid {
		env.Error("literal with void type", lit.where.Begin)
	}
}

func (lit *LiteralExpr) GetType() rs_api.Type {
	return lit.Type
}

func (lit *LiteralExpr) Generate(env CompilerEnv, byteCode ByteCode) bool {
	switch lit.Type {
	case rs_api.TypeInt:
		index, err := env.ConstPool().PushInt(lit.IntValue)
		if err != nil {
			env.Error(err.Error(), lit.where.Begin)
			return false
		}
		byteCode.LoadConstInt(index)
	case rs_api.TypeString:
		index, err := env.ConstPool().PushString(lit.StringValue)
		if err != nil {
			env.Error(err.Error(), lit.where.Begin)
			return false
		}
		byteCode.LoadConstString(index)
	default:
		env.Error("no load const opcode for type "+lit.Type.Name(), lit.where.Begin)
		return false
	}
	return true
}
