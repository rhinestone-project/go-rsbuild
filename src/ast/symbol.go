package ast

import "gitlab.com/rhinestone-project/go-rsbuild/src/util"

type SymbolType byte

const (
	SymbolTypeNone SymbolType = iota
	SymbolTypeVar
	SymbolTypeNativeType
)

type Symbol struct {
	Type SymbolType
	Name string
	Pos  util.Position
	Data interface{}
}

func NewSymbol(name string, t SymbolType) *Symbol {
	return &Symbol{
		Type: t,
		Name: name,
		Pos:  util.Position{},
		Data: nil,
	}
}
