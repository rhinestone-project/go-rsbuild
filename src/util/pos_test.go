package util

import (
	"reflect"
	"testing"
)

func TestFilePosition_CompareTo(t *testing.T) {
	t.Parallel()

	type fields struct {
		Row      int
		FilePath string
	}
	type args struct {
		other Comparable
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{
			name: "f1:100 vs f2:100",
			fields: fields{
				Row:      100,
				FilePath: "f1",
			},
			args: args{
				other: &FilePosition{
					Row:      100,
					FilePath: "f2",
				},
			},
			want: -1,
		},
		{
			name: "f1:100 vs f1:101",
			fields: fields{
				Row:      100,
				FilePath: "f1",
			},
			args: args{
				other: &FilePosition{
					Row:      101,
					FilePath: "f1",
				},
			},
			want: -1,
		},
		{
			name: "f1:100 vs f1:100",
			fields: fields{
				Row:      100,
				FilePath: "f1",
			},
			args: args{
				other: &FilePosition{
					Row:      100,
					FilePath: "f1",
				},
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fp := &FilePosition{
				Row:      tt.fields.Row,
				FilePath: tt.fields.FilePath,
			}
			if got := fp.CompareTo(tt.args.other); got != tt.want {
				t.Errorf("CompareTo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFilePosition_String(t *testing.T) {
	t.Parallel()

	type fields struct {
		Row      int
		FilePath string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "check",
			fields: fields{
				Row:      100,
				FilePath: "f1",
			},
			want: "f1:100",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fp := &FilePosition{
				Row:      tt.fields.Row,
				FilePath: tt.fields.FilePath,
			}
			if got := fp.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPositionRange_String(t *testing.T) {
	t.Parallel()

	type fields struct {
		Begin Position
		End   Position
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "check",
			fields: fields{
				Begin: Position{
					Offset: 0,
					Row:    1,
					Col:    1,
				},
				End: Position{
					Offset: 0,
					Row:    1,
					Col:    10,
				},
			},
			want: "[1:1]-[1:10]",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &PositionRange{
				Begin: tt.fields.Begin,
				End:   tt.fields.End,
			}
			if got := p.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPosition_CompareTo(t *testing.T) {
	t.Parallel()

	type fields struct {
		Offset int
		Row    int
		Col    int
	}
	type args struct {
		other Comparable
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		{
			name: "[1:1] vs [1:1]",
			fields: fields{
				Offset: 0,
				Row:    1,
				Col:    1,
			},
			args: args{
				other: &Position{
					Offset: 0,
					Row:    1,
					Col:    1,
				},
			},
			want: 0,
		},
		{
			name: "[1:1] vs [1:2]",
			fields: fields{
				Offset: 0,
				Row:    1,
				Col:    1,
			},
			args: args{
				other: &Position{
					Offset: 0,
					Row:    1,
					Col:    2,
				},
			},
			want: -1,
		},
		{
			name: "[1:1] vs [2:1]",
			fields: fields{
				Offset: 0,
				Row:    1,
				Col:    1,
			},
			args: args{
				other: &Position{
					Offset: 0,
					Row:    2,
					Col:    1,
				},
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Position{
				Offset: tt.fields.Offset,
				Row:    tt.fields.Row,
				Col:    tt.fields.Col,
			}
			if got := p.CompareTo(tt.args.other); got != tt.want {
				t.Errorf("CompareTo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPosition_RangeTo(t *testing.T) {
	t.Parallel()

	type fields struct {
		Offset int
		Row    int
		Col    int
	}
	type args struct {
		other Position
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   PositionRange
	}{
		{
			name: "check",
			fields: fields{
				Offset: 0,
				Row:    1,
				Col:    1,
			},
			args: args{
				other: Position{
					Offset: 0,
					Row:    2,
					Col:    1,
				},
			},
			want: PositionRange{
				Begin: Position{
					Offset: 0,
					Row:    1,
					Col:    1,
				},
				End: Position{
					Offset: 0,
					Row:    2,
					Col:    1,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Position{
				Offset: tt.fields.Offset,
				Row:    tt.fields.Row,
				Col:    tt.fields.Col,
			}
			if got := p.RangeTo(tt.args.other); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RangeTo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPosition_String(t *testing.T) {
	t.Parallel()

	type fields struct {
		Offset int
		Row    int
		Col    int
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "check",
			fields: fields{
				Offset: 0,
				Row:    1,
				Col:    1,
			},
			want: "[1:1]",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Position{
				Offset: tt.fields.Offset,
				Row:    tt.fields.Row,
				Col:    tt.fields.Col,
			}
			if got := p.String(); got != tt.want {
				t.Errorf("String() = %v, want %v", got, tt.want)
			}
		})
	}
}
