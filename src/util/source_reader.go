package util

import (
	"fmt"
	"io"
	"io/ioutil"
)

func ReadSourceData(source interface{}) ([]byte, error) {
	switch data := source.(type) {
	case []byte:
		return data, nil
	case *[]byte:
		return *data, nil
	case string:
		return []byte(data), nil
	case *string:
		return []byte(*data), nil
	case io.Reader:
		return ioutil.ReadAll(data)
	default:
		return nil, fmt.Errorf("unsupported source type: %T", source)
	}
}
