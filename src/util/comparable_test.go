package util

import (
	"strings"
	"testing"
)

type aType struct {
	int
}

type bType struct {
	string
}

func (a *aType) CompareTo(other Comparable) int {
	if o, ok := other.(*aType); ok {
		return a.int - o.int
	}
	panic(CannotCompareError(a, other))
}

func (b *bType) CompareTo(other Comparable) int {
	if o, ok := other.(*bType); ok {
		return strings.Compare(b.string, o.string)
	}
	panic(CannotCompareError(b, other))
}

func TestCannotCompareError(t *testing.T) {
	t.Parallel()

	type args struct {
		a Comparable
		b Comparable
	}
	tests := []struct {
		name    string
		args    args
		wantMsg string
		wantErr bool
	}{
		{
			name: "check",
			args: args{
				a: &aType{0},
				b: &bType{""},
			},
			wantMsg: "cannot compare *util.aType with *util.bType",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CannotCompareError(tt.args.a, tt.args.b); (err != nil) != tt.wantErr {
				t.Errorf("CannotCompareError() error = %v, wantErr %v", err, tt.wantErr)
			} else if err != nil && err.Error() != tt.wantMsg {
				t.Errorf("CannotCompareError() error message = %v, want message = %v", err.Error(), tt.wantMsg)
			}
		})
	}
}

func TestCompare(t *testing.T) {}

func Test_compare_CompareTo(t *testing.T) {
	t.Parallel()

	type fields struct {
		a Comparable
		b Comparable
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "0 CompareTo 10",
			fields: fields{
				a: &aType{0},
				b: &aType{10},
			},
			want: -10,
		},
		{
			name: "10 CompareTo 0",
			fields: fields{
				a: &aType{10},
				b: &aType{0},
			},
			want: 10,
		},
		{
			name: "10 CompareTo 10",
			fields: fields{
				a: &aType{10},
				b: &aType{10},
			},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &compare{
				a: tt.fields.a,
				b: tt.fields.b,
			}
			if got := c.CompareTo(); got != tt.want {
				t.Errorf("CompareTo() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_compare_Equals(t *testing.T) {
	t.Parallel()

	type fields struct {
		a Comparable
		b Comparable
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "1 == 1",
			fields: fields{
				a: &aType{1},
				b: &aType{1},
			},
			want: true,
		},
		{
			name: "1 == 10",
			fields: fields{
				a: &aType{1},
				b: &aType{10},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &compare{
				a: tt.fields.a,
				b: tt.fields.b,
			}
			if got := c.Equals(); got != tt.want {
				t.Errorf("Equals() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_compare_Great(t *testing.T) {
	t.Parallel()

	type fields struct {
		a Comparable
		b Comparable
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "1 > 1",
			fields: fields{
				a: &aType{1},
				b: &aType{1},
			},
			want: false,
		},
		{
			name: "10 > 1",
			fields: fields{
				a: &aType{10},
				b: &aType{1},
			},
			want: true,
		},
		{
			name: "1 > 10",
			fields: fields{
				a: &aType{1},
				b: &aType{10},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &compare{
				a: tt.fields.a,
				b: tt.fields.b,
			}
			if got := c.Great(); got != tt.want {
				t.Errorf("Great() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_compare_GreatOrEquals(t *testing.T) {
	t.Parallel()

	type fields struct {
		a Comparable
		b Comparable
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "1 >= 1",
			fields: fields{
				a: &aType{1},
				b: &aType{1},
			},
			want: true,
		},
		{
			name: "10 >= 1",
			fields: fields{
				a: &aType{10},
				b: &aType{1},
			},
			want: true,
		},
		{
			name: "1 >= 10",
			fields: fields{
				a: &aType{1},
				b: &aType{10},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &compare{
				a: tt.fields.a,
				b: tt.fields.b,
			}
			if got := c.GreatOrEquals(); got != tt.want {
				t.Errorf("GreatOrEquals() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_compare_Less(t *testing.T) {
	t.Parallel()

	type fields struct {
		a Comparable
		b Comparable
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "1 < 1",
			fields: fields{
				a: &aType{1},
				b: &aType{1},
			},
			want: false,
		},
		{
			name: "10 < 1",
			fields: fields{
				a: &aType{10},
				b: &aType{1},
			},
			want: false,
		},
		{
			name: "1 < 10",
			fields: fields{
				a: &aType{1},
				b: &aType{10},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &compare{
				a: tt.fields.a,
				b: tt.fields.b,
			}
			if got := c.Less(); got != tt.want {
				t.Errorf("Less() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_compare_LessOrEquals(t *testing.T) {
	t.Parallel()

	type fields struct {
		a Comparable
		b Comparable
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "1 <= 1",
			fields: fields{
				a: &aType{1},
				b: &aType{1},
			},
			want: true,
		},
		{
			name: "10 <= 1",
			fields: fields{
				a: &aType{10},
				b: &aType{1},
			},
			want: false,
		},
		{
			name: "1 <= 10",
			fields: fields{
				a: &aType{1},
				b: &aType{10},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &compare{
				a: tt.fields.a,
				b: tt.fields.b,
			}
			if got := c.LessOrEquals(); got != tt.want {
				t.Errorf("LessOrEquals() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_compare_NotEquals(t *testing.T) {
	t.Parallel()

	type fields struct {
		a Comparable
		b Comparable
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "1 != 1",
			fields: fields{
				a: &aType{1},
				b: &aType{1},
			},
			want: false,
		},
		{
			name: "10 != 1",
			fields: fields{
				a: &aType{10},
				b: &aType{1},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &compare{
				a: tt.fields.a,
				b: tt.fields.b,
			}
			if got := c.NotEquals(); got != tt.want {
				t.Errorf("NotEquals() = %v, want %v", got, tt.want)
			}
		})
	}
}
