package util

import (
	"bytes"
	"reflect"
	"testing"
)

func TestReadSourceData(t *testing.T) {
	t.Parallel()

	byteArray := []byte("source")
	str := "source"
	type args struct {
		source interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    []byte
		wantErr bool
	}{
		{
			name: "from []byte",
			args: args{
				source: []byte("source"),
			},
			want:    []byte("source"),
			wantErr: false,
		},
		{
			name: "from *[]byte",
			args: args{
				source: &byteArray,
			},
			want:    []byte("source"),
			wantErr: false,
		},
		{
			name: "from string",
			args: args{
				source: "source",
			},
			want:    []byte("source"),
			wantErr: false,
		},
		{
			name: "from *string",
			args: args{
				source: &str,
			},
			want:    []byte("source"),
			wantErr: false,
		},
		{
			name: "from io.Reader",
			args: args{
				source: bytes.NewBufferString("source"),
			},
			want:    []byte("source"),
			wantErr: false,
		},
		{
			name: "from unsupported source",
			args: args{
				source: 0,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadSourceData(tt.args.source)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadSourceData() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadSourceData() got = %v, want %v", got, tt.want)
			}
		})
	}
}
