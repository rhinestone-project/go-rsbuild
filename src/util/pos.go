package util

import (
	"strconv"
	"strings"
)

type Position struct {
	Offset, Row, Col int
}

func (p *Position) String() string {
	return "[" + strconv.Itoa(p.Row) + ":" + strconv.Itoa(p.Col) + "]"
}

func (p *Position) CompareTo(other Comparable) int {
	if o, ok := other.(*Position); ok {
		if p.Row == o.Row {
			return p.Col - o.Col
		}
		return p.Row - o.Row
	}
	panic(CannotCompareError(p, other))
}

func (p *Position) Line() int {
	return p.Row
}

type PositionRange struct {
	Begin, End Position
}

func (p *PositionRange) String() string {
	return p.Begin.String() + "-" + p.End.String()
}

func (p *Position) RangeTo(other Position) PositionRange {
	return PositionRange{
		Begin: *p,
		End:   other,
	}
}

type FilePosition struct {
	Row      int
	FilePath string
}

func (fp *FilePosition) String() string {
	return fp.FilePath + ":" + strconv.Itoa(fp.Row)
}

func (fp *FilePosition) CompareTo(other Comparable) int {
	if o, ok := other.(*FilePosition); ok {
		if fp.FilePath == o.FilePath {
			return fp.Row - o.Row
		}
		return strings.Compare(fp.FilePath, o.FilePath)
	}
	panic(CannotCompareError(fp, other))
}

func (fp *FilePosition) Line() int {
	return fp.Row
}
