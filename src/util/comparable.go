package util

import (
	"fmt"
)

type Comparable interface {
	CompareTo(other Comparable) int
}

type compare struct {
	a, b Comparable
}

func (c *compare) CompareTo() int {
	return c.a.CompareTo(c.b)
}

func (c *compare) Less() bool {
	return c.CompareTo() < 0
}

func (c *compare) LessOrEquals() bool {
	return c.CompareTo() <= 0
}

func (c *compare) Great() bool {
	return c.CompareTo() > 0
}

func (c *compare) GreatOrEquals() bool {
	return c.CompareTo() >= 0
}

func (c *compare) Equals() bool {
	return c.CompareTo() == 0
}

func (c *compare) NotEquals() bool {
	return c.CompareTo() != 0
}

func Compare(a, b Comparable) *compare {
	return &compare{a, b}
}

func CannotCompareError(a, b Comparable) error {
	return fmt.Errorf("cannot compare %T with %T", a, b)
}
