package util

import (
	"reflect"
	"testing"
)

func TestEscapedBytesOfRune(t *testing.T) {
	t.Parallel()

	type args struct {
		r rune
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "slash r",
			args: args{
				r: '\r',
			},
			want: []byte{'\\', 'r'},
		},
		{
			name: "slash t",
			args: args{
				r: '\t',
			},
			want: []byte{'\\', 't'},
		},
		{
			name: "slash a",
			args: args{
				r: '\a',
			},
			want: []byte{'\\', 'a'},
		},
		{
			name: "slash f",
			args: args{
				r: '\f',
			},
			want: []byte{'\\', 'f'},
		},
		{
			name: "slash v",
			args: args{
				r: '\v',
			},
			want: []byte{'\\', 'v'},
		},
		{
			name: "slash b",
			args: args{
				r: '\b',
			},
			want: []byte{'\\', 'b'},
		},
		{
			name: "slash n",
			args: args{
				r: '\n',
			},
			want: []byte{'\\', 'n'},
		},
		{
			name: "slash 0",
			args: args{
				r: 0,
			},
			want: []byte{'\\', '0'},
		},
		{
			name: "slash slash",
			args: args{
				r: '\\',
			},
			want: []byte{'\\', '\\'},
		},
		{
			name: "slash single quote",
			args: args{
				r: '\'',
			},
			want: []byte{'\\', '\''},
		},
		{
			name: "slash quote",
			args: args{
				r: '"',
			},
			want: []byte{'\\', '"'},
		},
		{
			name: "not escape",
			args: args{
				r: 'G',
			},
			want: []byte("G"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EscapedBytesOfRune(tt.args.r); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EscapedBytesOfRune() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEscapedBytesOfString(t *testing.T) {
	t.Parallel()

	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want []byte
	}{
		{
			name: "normal",
			args: args{
				s: "it is text",
			},
			want: []byte("it is text"),
		},
		{
			name: "with escapes",
			args: args{
				"it \"is\" text",
			},
			want: []byte("it \\\"is\\\" text"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EscapedBytesOfString(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("EscapedBytesOfString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEscapedStringOfRune(t *testing.T) {
	t.Parallel()

	type args struct {
		r rune
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "normal",
			args: args{
				r: 'a',
			},
			want: "a",
		},
		{
			name: "escaped",
			args: args{
				r: '\a',
			},
			want: "\\a",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EscapedStringOfRune(tt.args.r); got != tt.want {
				t.Errorf("EscapedStringOfRune() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEscapedStringOfString(t *testing.T) {
	t.Parallel()

	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "normal",
			args: args{
				s: "it is text",
			},
			want: "it is text",
		},
		{
			name: "with escapes",
			args: args{
				"it \"is\" text",
			},
			want: "it \\\"is\\\" text",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := EscapedStringOfString(tt.args.s); got != tt.want {
				t.Errorf("EscapedStringOfString() = %v, want %v", got, tt.want)
			}
		})
	}
}
