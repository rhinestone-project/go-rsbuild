package util

func EscapedBytesOfRune(r rune) []byte {
	buf := make([]byte, 2)
	buf[0] = '\\'
	switch r {
	case '\r':
		buf[1] = 'r'
	case '\t':
		buf[1] = 't'
	case '\a':
		buf[1] = 'a'
	case '\f':
		buf[1] = 'f'
	case '\v':
		buf[1] = 'v'
	case '\b':
		buf[1] = 'b'
	case '\n':
		buf[1] = 'n'
	case 0:
		buf[1] = '0'
	case '\\':
		buf[1] = '\\'
	case '\'':
		buf[1] = '\''
	case '"':
		buf[1] = '"'
	default:
		return []byte(string(r))
	}
	return buf
}

func EscapedStringOfRune(r rune) string {
	return string(EscapedBytesOfRune(r))
}

func EscapedBytesOfString(s string) []byte {
	buf := make([]byte, 0, len(s)*2)
	for _, r := range s {
		buf = append(buf, EscapedBytesOfRune(r)...)
	}
	return buf
}

func EscapedStringOfString(s string) string {
	return string(EscapedBytesOfString(s))
}
