package bytecode

import (
	go_serialize "gitlab.com/egor9814/go-serialize"
	"gitlab.com/rhinestone-project/go-rsbuild/src/ast"
	"gitlab.com/rhinestone-project/go-rsvm/rs_api"
	"gitlab.com/rhinestone-project/go-rsvm/rs_bytecode"
)

type byteCode struct {
	data *[]byte
	out  *go_serialize.Serializer
}

func (bc *byteCode) addBytes(data []byte) {
	bc.out.MustBytes(data)
}

func (bc *byteCode) addUInt(value uint) {
	bc.out.MustUInt(value)
}

func (bc *byteCode) addInt(value int) {
	bc.out.MustInt(value)
}

func (bc *byteCode) addUInt32(value uint32) {
	bc.out.MustUInt32(value)
}

func (bc *byteCode) addString(value string) {
	bc.out.MustString(value)
}

func (bc *byteCode) addOpCode(code rs_bytecode.Type) {
	bc.out.MustByte(byte(code))
}

func (bc *byteCode) Data() []byte {
	return *bc.data
}

func (bc *byteCode) LoadConstInt(index uint) {
	bc.addOpCode(rs_bytecode.LoadConstInt)
	bc.addUInt(index)
}

func (bc *byteCode) LoadConstString(index uint) {
	bc.addOpCode(rs_bytecode.LoadConstString)
	bc.addUInt(index)
}

func (bc *byteCode) LoadInt(name string) {
	bc.addOpCode(rs_bytecode.LoadInt)
	bc.addString(name)
}

func (bc *byteCode) LoadString(name string) {
	bc.addOpCode(rs_bytecode.LoadString)
	bc.addString(name)
}

func (bc *byteCode) StoreInt(name string) {
	bc.addOpCode(rs_bytecode.StoreInt)
	bc.addString(name)
}

func (bc *byteCode) StoreString(name string) {
	bc.addOpCode(rs_bytecode.StoreString)
	bc.addString(name)
}

func (bc *byteCode) PopInt() {
	bc.addOpCode(rs_bytecode.PopInt)
}

func (bc *byteCode) PopString() {
	bc.addOpCode(rs_bytecode.PopString)
}

func (bc *byteCode) DubInt() {
	bc.addOpCode(rs_bytecode.DupInt)
}

func (bc *byteCode) DubString() {
	bc.addOpCode(rs_bytecode.DupString)
}

func (bc *byteCode) InvokeNative(index rs_api.Index) {
	bc.addOpCode(rs_bytecode.InvokeNative)
	bc.addUInt32(uint32(index))
}

func (bc *byteCode) End() {
	bc.addOpCode(rs_bytecode.End)
}

func New() ast.ByteCode {
	out, data := go_serialize.NewByteArraySerializer(4)
	return &byteCode{
		data: data,
		out:  out,
	}
}
