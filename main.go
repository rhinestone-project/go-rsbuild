package main

import (
	"fmt"
	"gitlab.com/rhinestone-project/go-rsbuild/src/project"
	"os"
)

func main() {
	err := project.Execute(os.Args[1:])
	if err != nil {
		fmt.Println("error:", err.Error())
		os.Exit(1)
	}
}
